﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.IOS.Commands
{
   public class TwoFingerTapArgument: CommandArgumentBase
    {
        public TwoFingerTapArgument() : base() { }
    }

    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "TwoFingerTap")]
    public class TwoFingerTap : IOSReactUICommandBase<TwoFingerTapArgument>
    {
        protected override object OnReactElement(TwoFingerTapArgument argument, IOSElement element, AppiumDriver<IOSElement> driver, IStepInstance step)
        {
            var dicParam = new Dictionary<string, string>();
            dicParam.Add("element", element.Id);
            driver.ExecuteScript("mobile: twoFingerTap", dicParam);
            return null;
        }

        public override string Name
        {
            get
            {
                return "TwoFingerTap";
            }
        }
    }
}
