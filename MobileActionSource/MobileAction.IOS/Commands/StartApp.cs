﻿using MobileAction.Base;
using MobileAction.Commands;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.IOS.Commands
{
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "StartApp")]
   public class StartApp : StartAppBase<StartAppArgument,IOSElement>
    {
        protected override Type GetDriverType()
        {
            return typeof(IOSDriver<IOSElement>);
        }

        protected override DesiredCapabilities InitDesiredCapabilities(StartAppArgument args)
        {
            var _desiredCapabilites = new DesiredCapabilities();
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformVersion, args.PlatformVersion);
            _desiredCapabilites.SetCapability(MobileCapabilityType.DeviceName, args.DeviceName);
            _desiredCapabilites.SetCapability(MobileCapabilityType.App, args.AppPackage);
            _desiredCapabilites.SetCapability(MobileCapabilityType.AutomationName, "Appium");
            _desiredCapabilites.SetCapability(MobileCapabilityType.Udid, args.UDID);
            _desiredCapabilites.SetCapability(IOSMobileCapabilityType.BundleId, "com.hrcloud.Workmates");
            _desiredCapabilites.SetCapability("useNewWDA", true);
            _desiredCapabilites.SetCapability(MobileCapabilityType.NewCommandTimeout, 9999);
            _desiredCapabilites.SetCapability(IOSMobileCapabilityType.SendKeyStrategy, "grouped");
            _desiredCapabilites.SetCapability("connectHardwareKeyboard", true);
            _desiredCapabilites.SetCapability("maxTypingFrequency", 800);
            return _desiredCapabilites;
        }
    }
}
