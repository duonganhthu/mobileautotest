﻿using MobileAction.Base;
using OpenQA.Selenium.Appium.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.IOS.Commands
{
    public abstract class IOSReactUICommandBase<T>: ReactUICommandBase<T,IOSElement> where T: CommandArgumentBase
    {
    }
}
