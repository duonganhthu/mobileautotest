﻿using AutoTest.Data;
using OpenQA.Selenium.Appium.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.IOS
{
    public class IOSAction : MobileAction<IOSElement>
    {
        public IOSAction():base()
        {
        }

        public IOSAction(MobileAction<IOSElement> action) : base(action)
        {
        }

        public override string Name
        {
            get
            {
                return "IOSAction";
            }
        }

        public override ActionBase Clone()
        {
            return new IOSAction(this);
        }
    }
}
