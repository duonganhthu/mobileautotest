﻿using System;
using AutoTest.Data;
using MobileAction.Base;
using MobileAction.DataStructure;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Controls;

namespace MobileAction.IOS
{
    public class IOSActionManager : ActionManager
    {
        private static Dictionary<string,MobileCommand> _newAvailableCommands = Utilities.GetAllAvailableCommands<IOSElement>(Assembly.GetExecutingAssembly());
        public override string Name
        {
            get
            {
                return "IOS Action";
            }
        }

        public override ActionBase CreateAction()
        {
            return new IOSAction();
        }
        private MobileActionUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new MobileActionUI();
            _ui.Bind(action as IOSAction,_newAvailableCommands);
            return _ui;
        }

        public override Type[] RefTypes()
        {
            return new [] {typeof(StringBuilder)};
        }
    }
}
