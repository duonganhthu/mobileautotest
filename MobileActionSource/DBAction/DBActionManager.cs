﻿using AutoTest.Data;
using DbAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DBAction
{
    public class DBActionManager : ActionManager
    {
        public override string Name
        {
            get { return "DB Command"; }
        }

        public override ActionBase CreateAction()
        {
            return new DbAction.DBAction();
        }

        private static DBActionUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new DBActionUI();
            return _ui;
        }
    }
}
