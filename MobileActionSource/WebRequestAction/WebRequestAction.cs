﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebRequestAction
{
    public class WebRequestAction : ActionBase
    {
        public override string Name
        {
            get { return "Web Request"; }
        }

        public override IActionState State
        {
            get { return new SimpleActionState(); }
        }

        public override ActionBase Clone()
        {
            throw new NotImplementedException();
        }

        public override void Execute(IStepInstance step)
        {
        }
    }
}
