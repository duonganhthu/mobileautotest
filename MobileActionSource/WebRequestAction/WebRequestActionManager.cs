﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebRequestAction
{
    public class WebRequestActionManager : ActionManager
    {
        public override string Name
        {
            get { return "Web Request"; }
        }

        public override ActionBase CreateAction()
        {
            return new WebRequestAction();
        }

        public override System.Windows.Controls.UserControl UI(ActionBase action)
        {
            return new System.Windows.Controls.UserControl();
        }
    }
}
