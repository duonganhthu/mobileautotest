﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ExternalAction
{
    public class ExternalActionManager : ActionManager
    {
        public override string Name
        {
            get { return "External Function"; }
        }

        public override ActionBase CreateAction()
        {
            return new ExternalAction();
        }

        private static ExternalActionUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new ExternalActionUI();
            _ui.Bind(action as ExternalAction);
            return _ui;
        }
    }
}
