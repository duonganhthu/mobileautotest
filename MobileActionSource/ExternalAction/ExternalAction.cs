﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExternalAction
{
    public class ExternalAction : ActionBase
    {
        private const string Folder = "ExternalFunctions";
        private static Dictionary<string, AssemblyWarp> __AssembliesLib;
        internal static Dictionary<string, AssemblyWarp> AssembliesLib
        {
            get
            {
                if (__AssembliesLib == null)
                    __AssembliesLib = getAssemblies<ExternalLibAttribute>();

                return __AssembliesLib;
            }
        }

        private static Dictionary<string, AssemblyWarp> __AssembliesUnitTest;
        internal static Dictionary<string, AssemblyWarp> AssembliesUnitTest
        {
            get
            {
                if (__AssembliesUnitTest == null)
                    __AssembliesUnitTest = getAssemblies<UnitTestAttribute>();

                return __AssembliesUnitTest;
            }
        }


        private static IStepInstance _CurrentStep;
        public static IStepInstance CurrentStep
        {
            get
            {
                return _CurrentStep;
            }
        }

        public override string Name
        {
            get { return "External Function"; }
        }

        public override void Execute(IStepInstance step)
        {
            try
            {
                _CurrentStep = step;
                Dictionary<string, AssemblyWarp> assemblies = _State.IsUnitTest ? AssembliesUnitTest : AssembliesLib;
                AssemblyWarp assWarp = assemblies.ContainsKey(_State.AssemblyName) ? assemblies[_State.AssemblyName] : null;
                if (assWarp == null)
                    throw new Exception(string.Format("Assembly [{0}] was not loaded", _State.AssemblyName));

                ClassWarp type = assWarp.Classes.ContainsKey(_State.ClassName) ? assWarp.Classes[_State.ClassName] : null;
                if (type == null)
                    throw new Exception(string.Format("Can not find class [{0}] in assembly [{1}]", _State.ClassName, _State.AssemblyName));

                FunctionWarp func = type.Functions.ContainsKey(_State.FunctionName) ? type.Functions[_State.FunctionName] : null;
                if (func == null)
                    throw new Exception(string.Format("Can not find function [{0}] in class [{1}]", _State.FunctionName, _State.ClassName));

                List<Value> paras = null;
                if (_State.Variables != null && _State.Variables.Count > 0)
                {
                    paras = new List<Value>();
                    foreach (var e in _State.Variables)
                        paras.Add(step.StepData[e.Name]);
                }

                object instance = Activator.CreateInstance(type.Value, null);
                step.Result = func.Value.Invoke(instance, paras == null ? null : paras.ToArray());
            }
            catch (Exception ex)
            {
                step.Fail(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        private ExternalActionState _State = new ExternalActionState();
        public override IActionState State
        {
            get { return _State; }
        }

        public override ActionBase Clone()
        {
            ExternalAction res = new ExternalAction();
            res._State = (ExternalActionState)_State.Clone();
            return res;
        }

        private static Dictionary<string, AssemblyWarp> getAssemblies<T>() where T : AutoTestAttribute
        {
            Dictionary<string, AssemblyWarp> res = new Dictionary<string, AssemblyWarp>();

            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + Folder);
            string dataDll = Path.GetFileName(typeof(IStepInstance).Assembly.Location);
            string externalFunctionDll = Path.GetFileName(typeof(ExternalLibAttribute).Assembly.Location);
            foreach (string path in files.Where(e => e != dataDll && e != externalFunctionDll))
            {
                string ext = Path.GetExtension(path);
                string fileName = Path.GetFileName(path);

                if (fileName == dataDll || fileName == externalFunctionDll ||
                    (!string.Equals(".exe", ext, StringComparison.OrdinalIgnoreCase) && !string.Equals(".dll", ext, StringComparison.OrdinalIgnoreCase)))
                    continue;

                Assembly assembly = Assembly.LoadFrom(path);

                foreach (Type type in assembly.GetTypes())
                {
                    T att = type.GetCustomAttribute(typeof(T)) as T;
                    if (att != null)
                    {
                        AssemblyWarp assWarp = res.ContainsKey(assembly.FullName) ? res[assembly.FullName] : null;
                        if (assWarp == null)
                        {
                            assWarp = new AssemblyWarp(assembly);
                            res[assWarp.Key] = assWarp;
                        }

                        ClassWarp classWarp = new ClassWarp(att.FriendlyName, type);
                        assWarp.Classes[classWarp.Key] = classWarp;
                    }
                }
            }

            return res;
        }
    }

}
