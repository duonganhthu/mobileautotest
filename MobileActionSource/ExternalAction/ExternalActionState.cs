﻿using AutoTest.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;

namespace ExternalAction
{
    internal class ExternalActionState : IActionState
    {
        public string AssemblyName;
        public string ClassName;
        public string FunctionName;
        internal bool IsUnitTest;

        public List<Variable> Variables;

        public Dictionary<string, Value> GetStepParas()
        {
            Dictionary<string, Value> res = new Dictionary<string, Value>();
            if (Variables != null)
                foreach (Variable e in Variables)
                    res[e.Name] = new Value(e.Value);

            return res;
        }

        public IActionState Clone()
        {
            ExternalActionState res = new ExternalActionState();
            res.AssemblyName = this.AssemblyName;
            res.ClassName = this.ClassName;
            res.FunctionName = this.FunctionName;
            if (Variables != null)
            {
                res.Variables = new List<Variable>();
                foreach (Variable e in Variables)
                    res.Variables.Add(e.Clone());
            }
            return res;
        }

        public XElement SaveXml()
        {
            XElement res = new XElement("Action", new XAttribute("Type", typeof(ExternalAction).FullName));
            XElement xState = new XElement("States");
            res.Add(xState);

            xState.Add(new XElement("Assembly", this.AssemblyName));
            xState.Add(new XElement("Class", this.ClassName));
            xState.Add(new XElement("Function", this.FunctionName));
            xState.Add(new XElement("IsUT", this.IsUnitTest));

            if (Variables != null)
            {
                XElement xVars = new XElement("Variables");
                xState.Add(xVars);
                foreach (Variable e in Variables)
                    xVars.Add((e as IPersistent).SaveXml());
            }

            return res;
        }

        public void LoadXml(XElement parentNode)
        {
            var states = parentNode.Element("States");
            if (states != null)
            {
                this.AssemblyName = XmlUtility.GetXmlValue(states.Element("Assembly"), string.Empty);
                this.ClassName = XmlUtility.GetXmlValue(states.Element("Class"), string.Empty);
                this.FunctionName = XmlUtility.GetXmlValue(states.Element("Function"), string.Empty);
                this.IsUnitTest = XmlUtility.GetXmlValue<bool, XElement>(states.Element("IsUT"), false);
                XElement xVars = states.Element("Variables");
                if (xVars != null)
                {
                    this.Variables = new List<Variable>();
                    foreach (var e in xVars.Elements())
                    {
                        Variable res = new Variable();
                        this.Variables.Add(res);
                        (res as IPersistent).LoadXml(e);
                    }
                }
            }
        }

        public void OnVariableNameChanged(string oldValue, string newValue)
        {
            if (Variables != null)
            {
                Regex reg = new Regex("(?<={+)" + oldValue + "(?=}+)");

                foreach (Variable v in Variables)
                    if (v.Value is string || v.Value is Value)
                        v.Value = reg.Replace((string)v.Value, newValue);
            }
        }
    }
}
