﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestRail.TestRailArguments;

namespace TestRail.TestRailCommand
{
    public class TestRailCommand
    {
        public TestRailCommand(TestRailType action, Type actionArgs)
        {
            Action = action;
            ActionArgs = actionArgs;
        }

        public TestRailType Action { get; set; }

        public Type ActionArgs { get; set;}

        public static readonly List<TestRailCommand> AllCommands = new List<TestRailCommand>
        {
            new TestRailCommand(TestRailType.UpdateTestRail, typeof(UpdateTestRailArgs))
        };
    };
}
