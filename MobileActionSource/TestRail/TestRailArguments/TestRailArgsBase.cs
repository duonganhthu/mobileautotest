﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRail.TestRailArguments
{
    public abstract class TestRailArgsBase
    {
        public TestRailArgsBase()
        { }
        public TestRailArgsBase(IMemory data)
        {
            var properties = this.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (var prop in properties)
            {
                try
                {
                    if (prop.PropertyType == typeof(Enum) || prop.PropertyType.BaseType == typeof(Enum))
                    {
                        prop.SetValue(this, Enum.Parse(prop.PropertyType, data[prop.Name].ToString()));
                    }
                    else
                    {
                        prop.SetValue(this, Convert.ChangeType(data[prop.Name].ToString(), prop.PropertyType));
                    }
                }
                catch (InvalidCastException ex)
                {
                }
            }
        }
    }
}
