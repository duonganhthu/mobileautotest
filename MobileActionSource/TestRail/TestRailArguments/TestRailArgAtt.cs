﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRail.TestRailArguments
{
    public class TestRailArgAtt : Attribute
    {
        public string Label { get; set; }

        public string Tooltip { get; set; }

        public string[] DataSource { get; set; }

        public object DefaultValue { get; set; }
    }
}
