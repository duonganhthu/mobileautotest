﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRail.TestRailArguments
{
    public class UpdateTestRailArgs : TestRailArgsBase
    {
        public UpdateTestRailArgs(IMemory data) : base(data)
        {
        }

        [TestRailArgAtt(Label = "TestRunId")]
        public string TestRunId { get; set; }

        [TestRailArgAtt(Label = "TestCaseId")]
        public string TestCaseId { get; set;}

        [TestRailArgAtt(Label = "UserName")]
        public string UserName { get; set; }

        [TestRailArgAtt(Label = "Password")]
        public string Password { get; set; }

        [TestRailArgAtt(Label = "Comment")]
        public string Comment { get; set; }

        [TestRailArgAtt(Label = "Status", DataSource = new[] { "Passed", "Blocked", "Untested", "Retest", "Failed" })]
        public string Status { get; set; }

        [TestRailArgAtt(Label = "Version")]
        public string Version { get; set;}
    }
}
