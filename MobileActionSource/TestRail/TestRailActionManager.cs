﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TestRail
{
    public class TestRailActionManager : ActionManager
    {
        public override string Name
        {
            get { return "Test Rail"; }
        }

        public override ActionBase CreateAction()
        {
            return new TestRailAction();
        }

        private TestRailUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new TestRailUI();
            _ui.Bind(action as TestRailAction);

            return _ui;
        }
    }
}
