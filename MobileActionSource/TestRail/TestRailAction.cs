﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Diagnostics;
using System.Windows.Controls;
using System.Net;
using TestRail.TestRailCommand;
using TestRail.TestRailArguments;
using System.Web.Script.Serialization;

namespace TestRail
{
    [Serializable]
    public class TestRailAction : ActionBase
    {
        public const string Action = "Test Rail";
        #region Properties

        public override string Name
        {
            get { return Action; }
        }

        internal SimpleActionState _State = new SimpleActionState();
        public override IActionState State
        {
            get { return _State; }
        }

        #endregion

        #region Methods

        public override void Execute(IStepInstance step)
        {
            var action = (string)step.StepData[Action];
            if (string.IsNullOrWhiteSpace(action)) return;

            step.Log(action != null ? string.Format("Action: {0}", action) : "Action is empty");

            var actionType = (TestRailType)Enum.Parse(typeof(TestRailType), action);

            step.Log("Executing: ", actionType, "...");
            switch (actionType)
            {
                case TestRailType.UpdateTestRail:
                    var args = new UpdateTestRailArgs(step.StepData);
                    this.testRail(args.TestRunId, args.TestCaseId, args.UserName, args.Password, args.Status,args.Comment,args.Version);
                    break;
            }
            step.Log("Executed");
        }

        private bool testRail(Value TestRunId, Value TestCaseId, Value Username, Value Password, Value testStatus, Value Comment, Value Version)
        {
            if (TestCaseId == null || TestRunId == null || Username == null || Password == null) return false;
            // init json result 
            var jsonObj = new Dictionary<string, object>();
            jsonObj.Add("status_id", (int)Enum.Parse(typeof(TestRailResult), testStatus.ToString()));
            jsonObj.Add("comment", Comment);
            jsonObj.Add("version", Version);
            var serializer = new JavaScriptSerializer();
            var jsonResult = serializer.Serialize(jsonObj);
            //Passed = 1, Blocked = 2, Untested = 3, Retest = 4, Failed = 5
            HttpWebRequest request;
            HttpWebResponse response;
            Exception ex = null;

            string auth = Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(
                    String.Format(
                        "{0}:{1}",
                        (string)Username,
                        (string)Password
                    )
                )
            );

            request = (HttpWebRequest)WebRequest.Create("https://neogov.testrail.com/index.php?/api/v2/add_result_for_case/" + (string)TestRunId + "/" + (string)TestCaseId);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.Headers.Add("Authorization", "Basic " + auth);
            request.Timeout = 200000;


            if (!string.IsNullOrEmpty(jsonResult))
            {
                byte[] block = Encoding.UTF8.GetBytes(jsonResult);
                request.GetRequestStream().Write(block, 0, block.Length);
            }


            response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw;
                }

                response = (HttpWebResponse)e.Response;
                ex = e;
            }
            response.Close();


            return true;
        }

        public override ActionBase Clone()
        {
            var res = new TestRailAction();
            res._State = _State.Clone() as SimpleActionState;
            return res;
        }

        public override XElement SaveXml()
        {
            XElement xAction = new XElement("Action", new XAttribute("Type", this.GetType().FullName)
                , new XElement("Name", this.Name));
            xAction.Add(this.State.SaveXml());
            return xAction;
        }

        public override void LoadXml(XElement element)
        {
            this.State.LoadXml(element);
        }
            #endregion
        }
}
