﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestRail.TestRailArguments;

namespace TestRail
{
    /// <summary>
    /// Interaction logic for TestRailUI.xaml
    /// </summary>
    public partial class TestRailUI : UserControl
    {
        private static TestRailUI _instance;
        private TestRailAction _testrailAction;
        private bool _isBinding;

        #region Constructors

        public TestRailUI()
        {
            InitializeComponent();
            cmbActions.SelectionChanged += CmbActions_SelectionChanged;
        }

        #endregion

        #region Properties

        private SimpleActionState State
        {
            get { return _testrailAction.State as SimpleActionState; }
        }

        public string Action
        {
            get { return this.State[TestRailAction.Action]; }
            set { this.State[TestRailAction.Action] = value; }
        }


        public static TestRailUI Instance
        {
            get { return _instance ?? (_instance = new TestRailUI()); }
        }

        #endregion

        public void Bind(TestRailAction testrailAction)
        {
            var temp = _testrailAction;
            _testrailAction = testrailAction;
            if (temp == null)
            {
                PopulateActionComboBox();
            }
            if (object.Equals(temp, testrailAction) && object.Equals(temp.State, testrailAction.State))
            {
                return;
            }

            _isBinding = true;

            uiStackParas.Children.Clear();

            string actionName = this.Action.ToString();
            cmbActions.SelectedIndex = -1;
            if (!string.IsNullOrEmpty(actionName))
            {
                bool stop = false;
                for (int i = 0; i < cmbActions.Items.Count && !stop; i++)
                {
                    if (stop = (cmbActions.Items[i] as TestRailCommand.TestRailCommand).Action.ToString().Equals(actionName))
                    {
                        cmbActions.SelectedIndex = i;
                    }
                }
            }

            _isBinding = false;
        }

        private void CmbActions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var command = cmbActions.SelectedItem as TestRailCommand.TestRailCommand;
            if (command != null)
            {
                SimpleActionState oldState = (SimpleActionState)this.State.Clone();

                if (!_isBinding)
                {
                    this.State.Clear();
                    this.Action = oldState[TestRailAction.Action];
                }

                uiStackParas.Children.Clear();
                this.Action = command.Action.ToString();

                if (command.ActionArgs != null)
                {
                    var args = command.ActionArgs.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).OrderBy(p => p.MetadataToken);
                    foreach (var arg in args)
                    {
                        string value = oldState[arg.Name];
                        var attributes = arg.GetCustomAttributes(typeof(TestRailArguments.TestRailArgAtt), false);
                        var actionArgAttribute = attributes.Length > 0 ? attributes[0] as TestRailArgAtt : null;
                        if (actionArgAttribute != null)
                        {
                            uiStackParas.Children.Add(new Label()
                            {
                                Content = actionArgAttribute.Label,
                                ToolTip = actionArgAttribute.Tooltip,
                                Foreground = Brushes.Gray
                            });

                            if (actionArgAttribute.DataSource == null)
                            {
                                var textBox = new TextBox()
                                {
                                    Text = !string.IsNullOrEmpty(this.State[arg.Name]) ? this.State[arg.Name] :
                                    actionArgAttribute.DefaultValue != null ? actionArgAttribute.DefaultValue.ToString() : string.Empty,
                                    Padding = new Thickness(2)
                                };
                                textBox.TextChanged += (s, eventArgs) => { this.State[arg.Name] = ((TextBox)s).Text; };

                                this.State[arg.Name] = !string.IsNullOrEmpty(value) ? value :
                                    actionArgAttribute.DefaultValue != null ? actionArgAttribute.DefaultValue.ToString() : string.Empty;
                                uiStackParas.Children.Add(textBox);
                            }
                            else
                            {
                                ComboBox cmb = new ComboBox();
                                cmb.Padding = new Thickness(4);
                                cmb.Width = 200;
                                cmb.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                                cmb.ItemsSource = actionArgAttribute.DataSource;
                                cmb.SelectedItem = !string.IsNullOrEmpty(value) ? value :
                                    actionArgAttribute.DefaultValue != null ? actionArgAttribute.DefaultValue : cmb.Items[0];

                                cmb.SelectionChanged += (s, eventArgs) => { this.State[arg.Name] = ((ComboBox)cmb).SelectedItem as string; };

                                this.State[arg.Name] = cmb.SelectedItem as string;

                                uiStackParas.Children.Add(cmb);
                            }
                        }
                    }
                }
            }
        }


        private void PopulateActionComboBox()
        {
            ListCollectionView commands = new ListCollectionView(TestRailCommand.TestRailCommand.AllCommands);
            
            cmbActions.ItemsSource = commands;
        }
    }
}
