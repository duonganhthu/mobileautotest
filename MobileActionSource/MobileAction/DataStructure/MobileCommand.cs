﻿
using System;

namespace MobileAction.DataStructure
{
    public class MobileCommand
    {
        public ActionGroup Group { get; set; }
        public string Name { get; set; }
        public Type commandType { get; set; }
    };
}
