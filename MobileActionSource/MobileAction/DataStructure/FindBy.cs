﻿namespace MobileAction.DataStructure
{
    public  static class FindBy
    {
        public const string Id = "Id";
        public const string XPath = "XPath";
        public const string LinkText = "LinkText";
        public const string PartialLinkText = "PartialLinkText";
        public const string AccessibilityId = "AccessibilityId";
    }
}
