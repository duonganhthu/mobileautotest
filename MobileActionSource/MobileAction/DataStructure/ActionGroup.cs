﻿namespace MobileAction.DataStructure
{
    public enum ActionGroup
    {
        Mouse,
        Keyboard,
        Set,
        Validate,
        Command
    }
}
