﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MobileAction.Base
{
    public abstract class MobileActionBase : ActionBase
    {
        public const string Action = "Action";
        public static TimeSpan TimeOut = new TimeSpan(0, 0, 10);
        protected ICommandBase currentCommand;
        protected SimpleActionState _state = new SimpleActionState();
        public override IActionState State
        {
            get
            {
                return _state;
            }
        }
        public abstract ICommandBase InitCommand(Type commandType);
        public override void Execute(IStepInstance step)
        {
            currentCommand.Execute(step);
        }

        public override XElement SaveXml()
        {
            XElement xAction = new XElement("Action", new XAttribute("Type", GetType().FullName)
                , new XElement("Name", this.Name));
            XElement xState = new XElement("States");
            xAction.Add(xState);

            foreach (var e in this.State.GetStepParas())
                xState.Add(new XElement("State",
                    new XAttribute("Key", e.Key),
                    new XAttribute("Value", e.Value)));
            return xAction;
        }
        public override void LoadXml(XElement element)
        {
            this.State.LoadXml(element);
        }
    }
}
