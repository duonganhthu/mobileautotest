﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Base
{
    public abstract class ArgumentAttribute : Attribute
    {
        public string Label { get; set; }
        public string Tooltip { get; set; }
        public string DefaultValue { get; set; }
        public string[] DataSource { get; set; }
    }
}
