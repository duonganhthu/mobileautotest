﻿using AutoTest.Data;
using MobileAction.DataStructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Base
{
    public abstract class ReactUICommandBase<T, E> : CommandBase<T, E> where T : CommandArgumentBase where E : IWebElement
    {
        public static TimeSpan TimeOut = new TimeSpan(0, 0, 10);
        protected WebDriverWait wait;
        public override void OnExecute(IStepInstance step, T argument, AppiumDriver<E> driver)
        {
            try
            {
                wait = InitWait(argument, driver);
                step.Log("On Find Element:");
                var element = OnFindElement(argument, driver, step);
                step.Log("On React Element:");
                step.Result = OnReactElement(argument, element, driver, step);
            }
            catch(Exception ex)
            {
                step.Fail(string.Format("Error : {0}  StackTrace: {1}", ex.Message, ex.InnerException));
            }
            
        }

        protected virtual E OnFindElement(T argument, AppiumDriver<E> driver, IStepInstance step)
        {
            return Utilities.WaitElementDisplay<E>(wait, argument.ElementId, argument.FindBy, step);
        }
        protected abstract object OnReactElement(T argument, E element, AppiumDriver<E> driver, IStepInstance step);
        protected WebDriverWait InitWait(T argument, AppiumDriver<E> driver)
        {
            var seconds = string.IsNullOrEmpty(argument.Seconds) ? 0 : Convert.ToInt32(argument.Seconds);
            TimeSpan timeOut = seconds > 0 ? TimeSpan.FromMilliseconds(1000 * seconds) : TimeOut;
            return new WebDriverWait(driver, timeOut);
        }


    }
}
