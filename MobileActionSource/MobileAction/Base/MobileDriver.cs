﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Base
{
    public class MobileDriver<E> where E : IWebElement
    {
        private static AppiumDriver<E> _currentDriver;
        private MobileDriver() { }
        public static AppiumDriver<E> GetInstance()
        {
            if (_currentDriver == null)
                throw new Exception("Driver does not init ");
            return _currentDriver;
        }

        public static void Init(DesiredCapabilities desired, Type type, Uri inputUri, TimeSpan timeSpan)
        {
            //if (_currentDriver != null) return;
            var constructor = type.GetConstructor(new Type[] { typeof(Uri), typeof(DesiredCapabilities), typeof(TimeSpan) });
            _currentDriver = constructor != null ? (AppiumDriver<E>)constructor.Invoke(new object[] { inputUri, desired, timeSpan }):_currentDriver;
        }

    }
}
