﻿using AutoTest.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MobileAction.Base
{

    public abstract class CommandBase<T, E> : ICommandBase where T : CommandArgument where E : IWebElement
    {
        public abstract string Name { get; }

        public virtual void Render(StackPanel container, SimpleActionState state, SimpleActionState previousState)
        {
            var properties = typeof(T).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (var property in properties)
            {
                ArgumentAttribute attribute = property.GetCustomAttributes(typeof(ArgumentAttribute), true).FirstOrDefault() as ArgumentAttribute;
                if (attribute == null)
                    continue;

                container.Children.Add(new Label()
                {
                    Content = attribute.Label,
                    ToolTip = attribute.Tooltip,
                    Foreground = Brushes.Gray
                });

                string value = previousState == null ? null : previousState[property.Name];
                value = String.IsNullOrEmpty(value) && !state.Keys.Contains(property.Name) ? attribute.DefaultValue ?? "" : state[property.Name];


                if (attribute.DataSource == null)
                    container.Children.Add(OnRenderArgumentPropertyText(value, (text) => { state[property.Name] = text; }));
                else
                {
                    value = string.IsNullOrEmpty(value) ? attribute.DataSource.FirstOrDefault() : value;
                    container.Children.Add(OnRenderArgumentPropertyCollection(attribute.DataSource, value, (text) => { state[property.Name] = text; }));
                }

                state[property.Name] = value;
            }
        }

        protected virtual TextBox OnRenderArgumentPropertyText(string value, Action<String> onDataChanged)
        {
            var textBox = new TextBox()
            {
                Text = value,
                Padding = new Thickness(2)
            };
            textBox.TextChanged += (s, eventArgs) => { onDataChanged.Invoke(((TextBox)s).Text); };

            return textBox;
        }

        protected virtual ComboBox OnRenderArgumentPropertyCollection(string[] dataSource, string value, Action<String> onDataChanged)
        {
            ComboBox cmb = new ComboBox();
            cmb.Padding = new Thickness(4);
            cmb.Width = 200;
            cmb.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            cmb.ItemsSource = dataSource;
            cmb.SelectedItem = value;
            cmb.SelectionChanged += (s, eventArgs) => { onDataChanged.Invoke(((ComboBox)cmb).SelectedItem as string); };

            return cmb;
        }
        protected virtual T getArgument(IStepInstance step)
        {
            var lstProperty = typeof(T).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            var constructor = typeof(T).GetConstructor(new Type[] { });
            var instance = (T)constructor.Invoke(null);
            foreach (var prop in lstProperty)
            {
                var value = step.StepData[prop.Name];
                prop.SetValue(instance, value.ToString());
            }
            return instance ?? default(T);
        }
        public abstract void OnExecute(IStepInstance step, T argument, AppiumDriver<E> driver);

        public virtual void Execute(IStepInstance step)
        {
            try
            {
                step.Log(string.Format("Command {0}:", this.Name));
                OnExecute(step, getArgument(step), MobileDriver<E>.GetInstance());
            }
            catch (Exception ex)
            {
                step.Fail("Error : " + ex.Message + " " + ex.StackTrace);
                step.Result = null;
            }
        }
    }
}
