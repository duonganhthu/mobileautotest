﻿using AutoTest.Data;
using MobileAction.DataStructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Base
{
    public static class Utilities
    {
        private static E FindElement<E>(AppiumDriver<E> appiumDriver, string findBy, string elementId) where E : IWebElement
        {
            switch (findBy)
            {
                case FindBy.AccessibilityId:
                   return appiumDriver.FindElementByAccessibilityId(elementId);
                default:
                  return  appiumDriver.FindElement(InitBy(elementId, findBy));
            }
        }
        public static E WaitElementDisplay<E>(WebDriverWait wait, string elementId, string findByParam, IStepInstance step) where E : IWebElement
        {
            if (string.IsNullOrEmpty(elementId) | string.IsNullOrEmpty(findByParam)) throw new Exception(string.Format("Input params cannot be null ElementId: {0}  FindBy: {1}", elementId, findByParam));

            step.Log("WaitElementDisplay...");
            E element = default(E);
            wait.Until<bool>((driver) =>
            {
                try
                {
                    var appiumDriver = driver as AppiumDriver<E>;
                    element = FindElement<E>(appiumDriver, findByParam, elementId);
                    return element.Equals(default(E)) ? false : element.Displayed;
                }
                catch
                {
                    return false;
                }
            });
            step.Log(element.Equals(default(E)) ? "WaitElementDisplay FAIL" : "WaitElementDisplay Success");
            return element;
        }
        public static E WaitElementExisted<E>(WebDriverWait wait, string elementId, string findByParam, IStepInstance step) where E : IWebElement
        {

            if (string.IsNullOrEmpty(elementId) | string.IsNullOrEmpty(findByParam)) throw new Exception(string.Format("Input params cannot be null ElementId: {0}  FindBy: {1}",elementId,findByParam));

            step.Log("WaitElementExisted...");
            E element = default(E);
            wait.Until<bool>((driver) =>
            {
                try
                {
                    var appiumDriver = driver as AppiumDriver<E>;
                    element = FindElement<E>(appiumDriver, findByParam, elementId);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
            step.Log(element.Equals(default(E)) ? "WaitElementExisted FAIL" : "WaitElementExisted Success");
            return element;
        }
        public static By InitBy(string elementId, string finby)
        {
            string valueToFind = elementId;
            By by = finby == FindBy.Id ? By.Id(valueToFind) : finby == FindBy.LinkText ? By.LinkText(valueToFind) :
                       finby == FindBy.XPath ? By.XPath(valueToFind) : By.PartialLinkText(valueToFind);
            return by;
        }
        public static bool WaitElementNotExisted<E>(WebDriverWait wait, string elementId, string findByParam, IStepInstance step) where E : IWebElement
        {
            bool result = true;
            step.Log("WaitElementNotExisted...");
            wait.Until<bool>((driver) =>
            {
                try
                {
                    var appiumDriver = driver as AppiumDriver<E>;
                    FindElement<E>(appiumDriver, findByParam, elementId);
                    return false;
                }
                catch
                {
                    return result = true;
                }
            });
            return result;
        }


        public static IEnumerable<Type> GetTypesWith<TAttribute>(Assembly executingAssembly, bool inherit)
                               where TAttribute : System.Attribute
        {
            return from t in executingAssembly.GetTypes()
                   where t.IsDefined(typeof(TAttribute), inherit)
                   select t;
        }

        public static IEnumerable<Type> GetTypesWith<T>(bool inherit)
                              where T : System.Attribute
        {
            return from a in AppDomain.CurrentDomain.GetAssemblies()
                   from t in a.GetTypes()
                   where t.IsDefined(typeof(T), inherit)
                   select t;
        }

        public static IEnumerable<Type> GetTypesWithInterface(Assembly assemblyInput, Type baseClass, bool inherit)
        {
            return assemblyInput.GetTypes()
                         .Where(type => type.GetInterfaces().Contains(baseClass) && !type.IsAbstract);

        }

        public static IEnumerable<Type> GetTypesWithName(Assembly executingAssembly, string ClassName)
        {
            return //from a in executingAssembly.GetTypes()
                   from t in executingAssembly.GetTypes()
                   where t.Name.Equals(ClassName)
                   select t;
        }


        
        public static Dictionary<string,MobileCommand> GetAllAvailableCommands<T>(Assembly excecutingAssembly)
        {
            var result = new Dictionary<string,MobileCommand>();
            var lstActionType = GetTypesWithInterface(excecutingAssembly, typeof(ICommandBase), true);
            var baseActionType = GetTypesWithInterface(Assembly.GetExecutingAssembly(), typeof(ICommandBase), true);
            var resultList = lstActionType.Union(baseActionType);
            foreach (var type in resultList)
            {
                // get att 
               var att =  type.GetCustomAttribute(typeof(ActionAtt)) as ActionAtt;
                if (att == null || result.Keys.Contains(att.Name)) continue;
                result[att.Name] = new MobileCommand() { Name = att.Name, commandType = type, Group = att.Group };
            }
            return result;
        }

    }
}
