﻿
using mobileFindBy = MobileAction.DataStructure.FindBy;

namespace MobileAction.Base
{
  public class CommandArgumentBase : CommandArgument
    {
        public CommandArgumentBase() : base() { }
        [ArgumentAtt(Label = "FindBy",DefaultValue = mobileFindBy.XPath, DataSource = new string[] { mobileFindBy.XPath, mobileFindBy.AccessibilityId,mobileFindBy.Id,mobileFindBy.LinkText,mobileFindBy.PartialLinkText})]
        public string FindBy { get; set; }
        [ArgumentAtt(Label ="ElementId")]
        public string ElementId { get; set; }
        [ArgumentAtt(Label = "Timeout in seconds",DefaultValue = "10")]
        public string Seconds { get; set; }

    }
}
