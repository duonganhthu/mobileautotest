﻿using MobileAction.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Base
{
   public class ActionAtt : Attribute
    {
        public string Name { get; set; }
        public ActionGroup Group { get; set;}
    }
}
