﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MobileAction.Base
{
   public interface ICommandBase
    {
         void Render(StackPanel container, SimpleActionState state, SimpleActionState previousState);
         void Execute(IStepInstance step);
         string Name { get;}
    }
}
