﻿using AutoTest.Data;
using MobileAction.Base;
using MobileAction.DataStructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace MobileAction
{
    public abstract class MobileAction<E> : MobileActionBase where E : IWebElement
    {
        private static Dictionary<string, MobileCommand> _availableCommands;
        public Dictionary<string, MobileCommand> AvailableCommands { get { if (_availableCommands == null) _availableCommands = Utilities.GetAllAvailableCommands<E>(this.GetType().Assembly); return _availableCommands; } }

        static MobileAction()
        {
        }

        public MobileAction():base()
        {
        }

        public override ICommandBase InitCommand(Type type)
        {
            if (type.IsGenericType)
            {
                type = type.MakeGenericType(new[] { typeof(E) });
            }
            return currentCommand = Activator.CreateInstance(type) as ICommandBase;
        }

        public MobileAction(MobileAction<E> action)
        {
            _state = action.State.Clone() as SimpleActionState;
        }
        public override void LoadXml(XElement element)
        {
            base.LoadXml(element);
            var actionName = _state["Action"];
            if (!string.IsNullOrEmpty(actionName))
            {
                InitCommand(AvailableCommands[actionName].commandType);
            }
        }
    }
}
