﻿using AutoTest.Data;
using MobileAction.DataStructure;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.Generic;
using MobileAction.Base;
using OpenQA.Selenium;

namespace MobileAction
{
    public partial class MobileActionUI : UserControl
    {
        private static MobileActionUI _instance;
        private MobileActionBase _mobileAction;
        private bool _isBinding;
        private Dictionary<string,MobileCommand> _currentCommandSource;
        private ICommandBase currentCommand;
        #region Constructors

        public MobileActionUI()
        {
            InitializeComponent();
            cmbActions.SelectionChanged += CmbActions_SelectionChanged;
        }

        #endregion

        #region Properties

        private SimpleActionState State
        {
            get { return _mobileAction.State as SimpleActionState; }
        }

        public string Action
        {
            get { return this.State["Action"]; }
            set { State["Action"] = value; }
        }


        public static MobileActionUI Instance
        {
            get { return _instance ?? (_instance = new MobileActionUI()); }
        }

        

        #endregion

        public void Bind<E>(MobileAction<E> mobileAction, Dictionary<string,MobileCommand> availableCommands) where E : IWebElement
        {
            if (object.Equals(_mobileAction, mobileAction))
                return;
            _mobileAction = null;
            if (!object.Equals(_currentCommandSource, availableCommands))
                BindMobileCommandDataSource2(_currentCommandSource = availableCommands);

            _mobileAction = mobileAction;
            _isBinding = true;

            uiStackParas.Children.Clear();

            string actionName = this.Action;
            cmbActions.SelectedIndex = -1;
            if (!string.IsNullOrEmpty(actionName))
            {
                bool stop = false;
                for (int i = 0; i < cmbActions.Items.Count && !stop; i++)
                {
                    var mobileCommand = cmbActions.Items[i] as MobileCommand;
                    if (stop = mobileCommand.Name.ToString().Equals(actionName))
                    {
                        cmbActions.SelectedIndex = i;
                        currentCommand = mobileAction.InitCommand(mobileCommand.commandType);
                    }
                }
            }

            _isBinding = false;
        }

       

        private void CmbActions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var mobileCommand = cmbActions.SelectedItem as MobileCommand;
            if (_mobileAction == null || mobileCommand == null)
                return;

            SimpleActionState oldState = (SimpleActionState)this.State.Clone();
            if(!_isBinding)
            {
                this.State.Clear();
                this.Action = oldState["Action"];
            }
            uiStackParas.Children.Clear();
            this.Action = mobileCommand.Name.ToString();
            currentCommand = _mobileAction.InitCommand(mobileCommand.commandType);
            currentCommand.Render(uiStackParas, this.State, oldState);
        }

     
        private void BindMobileCommandDataSource2(Dictionary<string,MobileCommand> lstMobileCommands)
        {
            ListCollectionView commands = new ListCollectionView(new List<MobileCommand> (lstMobileCommands.Values));
            commands.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
            cmbActions.ItemsSource = commands;
        }
    }
}
