﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using System;

namespace MobileAction.Commands
{
    public class DragAndDropArgument : CommandArgumentBase
    {
        public DragAndDropArgument() : base() { }
        [ArgumentAtt(Label = "offsetX")]
        public string offsetX { get; set; }

        [ArgumentAtt(Label = "offsetY")]
        public string offsetY { get; set; }
        [ArgumentAtt(Label = "delay time")]
        public string delayTime { get; set; }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "DragAndDrop")]
    public class DragAndDropBase<E> : ReactUICommandBase<DragAndDropArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "DragAndDrop"; } }
        protected override object OnReactElement(DragAndDropArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            if (element == null) return null;
            var location = element.Location;
            var size = element.Size;
            var actionTouch = new TouchAction(driver as IPerformsTouchActions);
            var customAction = new TouchAction(driver as IPerformsTouchActions);
            var fromX = location.X + size.Width / 2;
            var fromY = location.Y + size.Height / 2;
            int offsetX = string.IsNullOrEmpty(argument.offsetX) ? 0 : Convert.ToInt32(argument.offsetX);
            int offsetY = string.IsNullOrEmpty(argument.offsetY) ? 0 : Convert.ToInt32(argument.offsetY);
            int delayTime = string.IsNullOrEmpty(argument.delayTime) ? 0 : Convert.ToInt32(argument.delayTime);
            customAction.Press(fromX, fromY).Wait(delayTime).MoveTo(offsetX, offsetY).Release().Perform();
            return null;
        }
    }
}
