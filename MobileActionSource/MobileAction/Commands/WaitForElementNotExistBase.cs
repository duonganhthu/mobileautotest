﻿using MobileAction.Base;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoTest.Data;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class WaitElementNotExistArgument : CommandArgumentBase
    {
        public WaitElementNotExistArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "WaitForElementNotExist")]
    public class WaitForElementNotExistBase<E> :ReactUICommandBase<WaitForElementArgument, E> where E : IWebElement
    {
        public override string Name { get { return "WaitForElementNotExist"; } }
        public override void OnExecute(IStepInstance step, WaitForElementArgument argument, AppiumDriver<E> driver)
        {
            var wait = base.InitWait(argument, driver);
            step.Result = Utilities.WaitElementNotExisted<E>(wait,argument.ElementId,argument.FindBy,step);
        }
        protected override object OnReactElement(WaitForElementArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            throw new NotImplementedException();
        }
    }
}
