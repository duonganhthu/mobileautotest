﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Commands
{
    public class WaitForElementArgument : CommandArgumentBase
    {
        public WaitForElementArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "WaitForElement")]
    public class WaitForElementBase<E> : ReactUICommandBase<WaitForElementArgument, E> where E : IWebElement
    {
        public override string Name { get { return "WaitForElement"; } }
        public override void OnExecute(IStepInstance step, WaitForElementArgument argument, AppiumDriver<E> driver)
        {
            try
            {
                base.OnExecute(step, argument, driver);
            }
            catch (WebDriverTimeoutException timeout)
            {
                step.Result = null;
                step.Log("Timeout! " + timeout.Message);
            }
            catch(Exception ex)
            {
                step.Result = null;
                step.Log("ERROR: " +ex.Message + " "+ ex.StackTrace);
            }
        }
        protected override E OnFindElement(WaitForElementArgument argument, AppiumDriver<E> driver, IStepInstance step)
        {
            return Utilities.WaitElementExisted<E>(wait, argument.ElementId, argument.FindBy, step);
        }
        protected override object OnReactElement(WaitForElementArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            return element;
        }
    }
}
