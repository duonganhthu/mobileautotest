﻿using MobileAction.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoTest.Data;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium;
using System.Net.Http;
using System.Text.RegularExpressions;
using Renci.SshNet;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;

namespace MobileAction.Commands
{
    public class StartAppArgument : CommandArgument
    {
        public StartAppArgument() { }
        [ArgumentAtt(Label = "Device Name")]
        public string DeviceName { get; set; }

        [ArgumentAtt(Label = "PlatformVersion")]
        public string PlatformVersion { get; set; }

        [ArgumentAtt(Label = "AppPackage")]
        public string AppPackage { get; set; }

        [ArgumentAtt(Label = "Port")]
        public string Port { get; set; }
        [ArgumentAtt(Label = "UDID")]
        public string UDID { get; set; }
        [ArgumentAtt(Label = "FullReset")]
        public string FullReset { get; set; }

        [ArgumentAtt(Label = "ServerIP")]
        public string ServerIP { get; set; }
        [ArgumentAtt(Label = "UserName")]
        public string UserName { get; set; }
        [ArgumentAtt(Label = "Password")]
        public string Password { get; set; }

    }
    public abstract class StartAppBase<T,E> : CommandBase<T,E> where T : StartAppArgument where E : AppiumWebElement
    {
        public override string Name { get { return "StartApp"; } }
        protected virtual void InitDriver(T argument)
        {
           var desired =  InitDesiredCapabilities(argument);
            var uri = new Uri(argument.ServerIP + ":" + argument.Port + "/wd/hub");
            var serverIp = IsIPAddress(argument.ServerIP);
            if (!String.IsNullOrEmpty(serverIp) && !CheckingAppiumServerIsOn(uri).Result)
            {
                StartApiumServer(argument.Port, serverIp, argument.UserName, argument.Password);
            }
            MobileDriver<E>.Init(desired, GetDriverType(), uri, new TimeSpan(0, 10, 0));
        }

        protected abstract Type GetDriverType();

        private void StartApiumServer(string port, string ip, string userName, string password)
        {
            KeyboardInteractiveAuthenticationMethod kauth = new KeyboardInteractiveAuthenticationMethod(userName);
            PasswordAuthenticationMethod pauth = new PasswordAuthenticationMethod(userName, password);
            kauth.AuthenticationPrompt += (s, e) =>
            {
                foreach (Renci.SshNet.Common.AuthenticationPrompt prompt in e.Prompts)
                {
                    if (prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        prompt.Response = password;
                    }
                }
            };
            ConnectionInfo connectionInfo = new ConnectionInfo(ip, 22, userName, pauth, kauth);

          var  remoteClient = new SshClient(connectionInfo);
            remoteClient.KeepAliveInterval = TimeSpan.FromSeconds(60);
            remoteClient.Connect();
            IDictionary<Renci.SshNet.Common.TerminalModes, uint> termkvp = new Dictionary<Renci.SshNet.Common.TerminalModes, uint>();
            termkvp.Add(Renci.SshNet.Common.TerminalModes.ECHO, 53);

            ShellStream shellStream = remoteClient.CreateShellStream("xterm", 80, 24, 800, 600, 1024, termkvp);
            //Get logged in
            string rep = shellStream.Expect(new Regex(@"[$>]")); //expect user prompt
            // send command
            shellStream.WriteLine("Appium");
            rep = shellStream.Expect(new Regex(@"([$#>:])")); //expect password or user prompt
        }


        private async Task<bool> CheckingAppiumServerIsOn(Uri appiumUri)
        {
            try
            {
                var urlChecking = appiumUri.AbsoluteUri + "/sessions";
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlChecking);
                var response = await client.GetAsync(urlChecking);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private string IsIPAddress(string appiumURL)
        {
            Regex regex = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
            MatchCollection result = regex.Matches(appiumURL);
            return result[0].Value;
        }
            
        protected virtual DesiredCapabilities InitDesiredCapabilities(T args)
        {
            var _desiredCapabilites = new DesiredCapabilities();
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformVersion, args.PlatformVersion);
            _desiredCapabilites.SetCapability(MobileCapabilityType.DeviceName, args.DeviceName);
            _desiredCapabilites.SetCapability(MobileCapabilityType.App, args.AppPackage);
            _desiredCapabilites.SetCapability(MobileCapabilityType.AutomationName, "Appium");
            _desiredCapabilites.SetCapability(MobileCapabilityType.Udid, args.UDID);
            _desiredCapabilites.SetCapability(MobileCapabilityType.NewCommandTimeout, 9999);
            return _desiredCapabilites;
        }
        public override void Execute(IStepInstance step)
        {
           var startAppArg = getArgument(step);
            InitDriver(startAppArg);
            OnExecute(step, getArgument(step), MobileDriver<E>.GetInstance());
        }

        public override void OnExecute(IStepInstance step, T argument, AppiumDriver<E> driver)
        {
           
        }


    }
}
