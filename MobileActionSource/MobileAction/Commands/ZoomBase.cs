﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class ZoomArgument : CommandArgumentBase
    {
        public ZoomArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name ="Zoom")]
    public class ZoomBase<E> : ReactUICommandBase<ZoomArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "Zoom"; } }
        protected override object OnReactElement(ZoomArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            driver.Zoom(element);
            return null;
        }
    }
}
