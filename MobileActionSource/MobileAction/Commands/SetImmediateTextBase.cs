﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class SetImmediateTextBaseArgument : SetTextArgument
    {
        public SetImmediateTextBaseArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "SetImmediateText")]
    public class SetImmediateTextBase<E> : ReactUICommandBase<SetImmediateTextBaseArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "SetImmediateText"; } }
        protected override object OnReactElement(SetImmediateTextBaseArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            // custom handle input
            element.SetImmediateValue(argument.Value.Replace(" ", "%s"));
            return null;
        }
    }
}
