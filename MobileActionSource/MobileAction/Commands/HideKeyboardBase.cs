﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class HideKeyBoardArgument : CommandArgumentBase
    {
        public HideKeyBoardArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "HideKeyboard")]
    public class HideKeyBoardBase<E> : CommandBase<HideKeyBoardArgument, E> where E : IWebElement
    {
        public override string Name { get { return "HideKeyboard"; } }
        public override void OnExecute(IStepInstance step, HideKeyBoardArgument argument, AppiumDriver<E> driver)
        {
            driver.HideKeyboard();
        }
    }
}
