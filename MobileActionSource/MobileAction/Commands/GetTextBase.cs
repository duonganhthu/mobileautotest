﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class GetTextArgument : CommandArgumentBase
    {
        public GetTextArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name ="GetText")]
    public class GetTextBase<E> : ReactUICommandBase<GetTextArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "GetText"; } }
        protected override object OnReactElement(GetTextArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            return element.Text;
        }
    }
}
