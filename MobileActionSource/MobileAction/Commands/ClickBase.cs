﻿using MobileAction.Base;
using AutoTest.Data;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class ClickArgument : CommandArgumentBase
    {
        public ClickArgument(): base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command,Name = "Click")]
    public class ClickBase<E> : ReactUICommandBase<ClickArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "Click"; } }
        protected override object OnReactElement(ClickArgument argument, E element, AppiumDriver<E> driver,IStepInstance step)
        {
            element.Click();
            return null;
        }
    }
    
}
