﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;
using System.Collections.Generic;

namespace MobileAction.Commands
{
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "DoubleClick")]
    public class DoubleClickBase<E> : ReactUICommandBase<ClickArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "DoubleClick"; } }
        protected override object OnReactElement(ClickArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            driver.ExecuteScript("mobile: doubleTap", new Dictionary<string, string>() { { "element", element.Id } });
            return null;
        }
    }
}
