﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Commands
{
    public class PinchArgument : CommandArgumentBase
    {
        public PinchArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "Pinch")]
    public class PinchBase<E> : ReactUICommandBase<PinchArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "Pinch"; } }
        protected override object OnReactElement(PinchArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            driver.ExecuteScript("mobile: pinch", new Dictionary<string, string>() { { "element", element.Id }, { "scale", "0.5"}, { "velocity","1.1" } });
            return null;
        }
    }
}
