﻿using AutoTest.Data;
using MobileAction.Base;
using MobileAction.DataStructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Commands
{
    public class ScrollArgument : CommandArgumentBase
    {
        public ScrollArgument() : base() { }

        [ArgumentAtt(Label = "Destination FindBy", DataSource = new[] { "Id", "XPath" })]
        public string desFindBy { get; set; }
        [ArgumentAtt(Label = "Scroll to element")]
        public string ToElement { get; set; }

        [ArgumentAtt(Label = "Direction", DataSource = new[] { "Up", "Down" }, DefaultValue ="Down")]
        public string Direction { get; set; }

    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name ="ScrollTo")]
    public class ScrollBase<E> : ReactUICommandBase<ScrollArgument, E> where E : IWebElement
    {
        public override string Name { get { return "ScrollTo"; } }
        protected override object OnReactElement(ScrollArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            var elementId = (element as AppiumWebElement).Id;
            var dicParam = new Dictionary<string, object>();
            dicParam.Add("element", elementId);
            dicParam.Add("direction", argument.Direction.ToLower());
            if (string.IsNullOrEmpty(argument.ToElement))
            {
                driver.ExecuteScript("mobile:scroll", dicParam);
                return null;
            }
            var elementFindByXpath = Utilities.WaitElementExisted<E>(base.wait, argument.ToElement, argument.desFindBy, step);
            while (!elementFindByXpath.Displayed)
            {
                driver.ExecuteScript("mobile:scroll", dicParam);
                elementFindByXpath = Utilities.WaitElementExisted<E>(base.wait, argument.ToElement, argument.desFindBy, step);
            }

            return null;
        }
    }
}
