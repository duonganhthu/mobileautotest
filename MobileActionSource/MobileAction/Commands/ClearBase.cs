﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class ClearBaseArgument : CommandArgumentBase
    {
        public ClearBaseArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command ,Name = "Clear")]
    public class ClearBase<E> : ReactUICommandBase<ClearBaseArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "Clear"; } }
        protected override object OnReactElement(ClearBaseArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            element.Clear();
            return null;
        }
    }
}
