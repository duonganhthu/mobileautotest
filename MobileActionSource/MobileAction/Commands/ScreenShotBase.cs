﻿using MobileAction.Base;
using OpenQA.Selenium;
using AutoTest.Data;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class ScreenShotBaseArgument : CommandArgument
    {
        public ScreenShotBaseArgument() : base() { }
        [ArgumentAtt(Label = "FilePath")]
        public string FilePath { get; set; }
    }

    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name ="ScreenShot")]
    public class ScreenShotBase<E> : CommandBase<ScreenShotBaseArgument, E> where E : IWebElement
    {
        public override string Name { get { return "ScreenShot"; } }
        public override void OnExecute(IStepInstance step, ScreenShotBaseArgument argument, AppiumDriver<E> driver)
        {
            var screenShot = driver.GetScreenshot();
            screenShot.SaveAsFile(argument.FilePath, ScreenshotImageFormat.Jpeg);
        }
    }
}
