﻿using MobileAction.Base;
using AutoTest.Data;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class TapArgument : CommandArgumentBase
    {
        public TapArgument() : base() { }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "ElementTap")]
    public class ElementTapBase<E> : ReactUICommandBase<TapArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "ElementTap"; } }
        protected override object OnReactElement(TapArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            element.Tap(1,1);
            return null;
        }

    }

}
