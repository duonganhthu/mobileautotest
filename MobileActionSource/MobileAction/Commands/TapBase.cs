﻿using MobileAction.Base;
using System;
using AutoTest.Data;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;

namespace MobileAction.Commands
{
    
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "Tap")]
    public class TapBase<E> : ReactUICommandBase<TapArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "Tap"; } }
        protected override object OnReactElement(TapArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            var location = element.Location;
            var size = element.Size;
            driver.ExecuteScript("mobile: tap", new Dictionary<string, string>() { /*{ "element", element.Id },*/ {"x", (location.X + (size.Width/2)).ToString() }, { "y", (location.Y + (size.Height / 2)).ToString() } });
            return null;
        }

    }

}
