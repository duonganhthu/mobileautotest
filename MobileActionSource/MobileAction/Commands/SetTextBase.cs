﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace MobileAction.Commands
{
    public class SetTextArgument : CommandArgumentBase
    {
        public SetTextArgument() : base() { }

        [ArgumentAtt(Label = "Value")]
        public string Value { get; set; }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name ="SetText")]
    public class SetTextBase<E> : ReactUICommandBase<SetTextArgument, E> where E : AppiumWebElement
    {
        public override string Name { get { return "SetText"; } }
        protected override object OnReactElement(SetTextArgument argument, E element, AppiumDriver<E> driver, IStepInstance step )
        {
            element.SendKeys(argument.Value);
            return null;
        }
    }
}
