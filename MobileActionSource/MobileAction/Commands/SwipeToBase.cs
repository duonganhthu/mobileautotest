﻿using AutoTest.Data;
using MobileAction.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System;

namespace MobileAction.Commands
{
    public class SwipeToArgument : CommandArgumentBase
    {
        public SwipeToArgument() : base() { }

        [ArgumentAtt(Label = "Destination FindBy", DataSource = new[] { "Id", "XPath" })]
        public string desFindBy { get; set; }
        [ArgumentAtt(Label = "Swipe to element")]
        public string ToElement { get; set; }

        [ArgumentAtt(Label = "Direction", DataSource = new[] { "Left", "Right" }, DefaultValue = "Left")]
        public string Direction { get; set; }

    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "Swipe")]
    public class SwipeToBase<E> : ReactUICommandBase<SwipeToArgument, E> where E : IWebElement
    {
        public override string Name { get { return "Swipe"; } }
        protected override  object OnReactElement(SwipeToArgument argument, E element, AppiumDriver<E> driver, IStepInstance step)
        {
            if (element == null) return null;
            var sourceLocation = element.Location;

            int sourceX = sourceLocation.X;
            int sourceY = sourceLocation.Y;
            // swipe from most left to most right or vice versal
            if (String.IsNullOrEmpty(argument.desFindBy))
            {
                var sourceSize = element.Size;
                if (argument.Direction.ToLower().Equals("left"))
                {
                    driver.Swipe(sourceX + sourceSize.Width, sourceY + (sourceSize.Height / 2), sourceX, sourceY + (sourceSize.Height / 2), 1);
                    return null;
                }
                driver.Swipe(sourceX, sourceY + (sourceSize.Height / 2), sourceX + sourceSize.Width, sourceY + (sourceSize.Height / 2), 1);
                return null;
            }

            var destElement = Utilities.WaitElementExisted<E>(wait, argument.ToElement,argument.desFindBy,step);
            if (destElement == null) return null;
            var destLocation = destElement.Location;
            int destX = destLocation.X;
            int destY = destLocation.Y;
            driver.Swipe(sourceX, sourceY, destX, destY, 1);
            return null;
        }
    }
}
