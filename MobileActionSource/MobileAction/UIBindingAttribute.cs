﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction
{
    public class UIBindingAttribute : Attribute
    {
        public string Label { get; set; }

        public string Tooltip { get; set; }

        public string[] DataSource { get; set; }

        public object DefaultValue { get; set; }
    }
}
