﻿function record(startStop) {
    isRecording = startStop == true;
}

function getElementXPath(element) {
    if (element && element.id)
        return '//*[@id="' + element.id + '"]';
    else
        return getElementTreeXPath(element);
}

function getElementTreeXPath(element) {
    var paths = [];
    for (; element && element.nodeType == 1; element = element.parentNode) {
        var index = 0;
        for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling) {
            if (sibling.nodeType == 10)
                continue;

            if (sibling.nodeName == element.nodeName)
                ++index;
        }

        var tagName = element.nodeName.toLowerCase();
        var pathIndex = (index ? "[" + (index + 1) + "]" : "");
        paths.splice(0, 0, tagName + pathIndex);
    }

    return paths.length ? "/" + paths.join("/") : null;
}

function stopEvent(e) {
    e = e || window.event;
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation)
        e.stopPropagation();
    if (e.preventDefault)
        e.preventDefault();
}

var eventType = {
    Click: 'Click',
    DoubleClick: 'DoubleClick',
    MouseDown: 'MouseDown',
    MouseUp: 'MouseUp',
    MouseMove: 'MouseMove',
    MouseOut: 'MouseOut',
    MouseOver: 'MouseOver',
    Dragdrop: 'Dragdrop',
    KeyDown: 'KeyDown',
    KeyPress: 'KeyPress',
    KeyUp: 'KeyUp',
    SystemKeyDown: 'SystemKeyDown',
    SystemKeyUp: 'SystemKeyUp',
    SwitchTo: 'SwitchTo',
    SwitchToDefaultContent: 'SwitchToDefaultContent',
    TypeText: 'TypeText',

    SetText: 'SetText',
    SetCheckBoxValue: 'SetCheckBoxValue',
    SetDropDownItem: 'SetDropDownItem',
    GetText: 'GetText',
    GetElement: 'GetElement',
    GetPageTitle: 'GetPageTitle',

    LaunchApp: 'LaunchApp',
    Navigate: 'Navigate',
    Confirm: 'Confirm',
    CloseWindow: 'CloseWindow',
    JavaScriptJavaScript: 'JavaScriptJavaScript'
}

var findBy = {
    Id: 'Id',
    Name: 'Name',
    TagName: 'TagName',
    ClassName: 'ClassName',
    CssSelector: 'CssSelector',
    XPath: 'XPath',
    LinkText: 'LinkText',
    PartialLinkText: 'PartialLinkText'
}

var paras = {
    FindBy: 'FindBy',
    ObjectId: 'ObjectIdentify',
    Condition: 'Condition',
    X: 'X',
    Y: 'Y',
    Button: 'Button',
    StringValue: 'StringValue',
    ScriptValue: 'ScriptValue',
    SystemKey: 'SystemKey',
    Browser: 'Browser',
    Url: 'Url',
    RelativeUrl: 'RelativeUrl',
    FindTargetBy: 'FindTargetBy',
    ObjectTargetId: 'ObjectTargetId',
    IsChecked: 'IsChecked',
    TimeOut: 'TimeOut'
}

window.attachEvent("onload", function () {
    document.body.attachEvent("onclick", function (e) {
        var id = getElementXPath(e.srcElement)
        if (isRecording) {
            var data = {
                StepTitle: 'Click on: ' + id,
                Event: eventType.Click,
                FindBy: findBy.Id
            }
            data[paras.ObjectId] = id;
            data[paras.X] = e.x;
            data[paras.Y] = e.y;

            alert(JSON.stringify(data));
        }
        else {
            window.external.Spy(id);
            stopEvent(e);
        }
    }, false);

    var eles = document.getElementsByTagName("input") || [];
    for (var i = 0; i < eles.length; i++)
        eles[i].attachEvent("onblur", function (e) {
            if (e.srcElement.attributes["TYPE"].nodeValue != 'text')
                return;

            var id = getElementXPath(e.srcElement)
            if (isRecording) {
                var data = {
                    StepTitle: 'Set text: ' + id,
                    Event: eventType.SetText,
                    FindBy: findBy.Id
                }
                data[paras.ObjectId] = id;
                data[paras.StringValue] = e.srcElement.value;

                alert(JSON.stringify(data));
            }
            else {
                window.external.Spy(id);
                stopEvent(e);
            }
        }, false);
});
