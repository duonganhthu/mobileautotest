﻿using AutoTest.Data;
using MobileAction.Base;
using MobileAction.DataStructure;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Controls;

namespace MobileAction.Android
{
    public class AndroidActionManager : ActionManager
    {
        private static Dictionary<string, MobileCommand> _newAvailableCommands = Utilities.GetAllAvailableCommands<AndroidElement>(Assembly.GetExecutingAssembly());
        public override string Name
        {
            get
            {
                return "Android Action";
            }
        }

        public override ActionBase CreateAction()
        {
            return new AndroidAction();
        }
        private MobileActionUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new MobileActionUI();
            _ui.Bind(action as AndroidAction, _newAvailableCommands);
            return _ui;
        }

        public override Type[] RefTypes()
        {
            return new[] { typeof(StringBuilder) };
        }

    }
}
