﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.Linq;
using MobileAction.ActionArguments;
using MobileAction.DataStructure;
using OpenQA.Selenium;
using System.Drawing;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.iOS;

namespace MobileAction.Android
{
    public class AndroidDriverAdapter : AdapterBase<AndroidElement>, IAndroidDriverAdapter
    {
        public override void TypeText(IWebElement element, TypeTextActionArgs args)
        {
            element.SendKeys(element.Text + args.Value);
        }

        public void ReplaceValue(AndroidElement element, string value)
        {
            element.ReplaceValue(value);
        }

        public override void StopApp(StopAppActionArgs args)
        {            
            if (Driver == null) return;
            Driver.Quit();            
        }

        public void ConnectInternet()
        {
            (Driver as AndroidDriver<AndroidElement>).ConnectionType = ConnectionType.WifiOnly;
        }

        public void DisconnectInternet()
        {
            (Driver as AndroidDriver<AndroidElement>).ConnectionType = 0;
        }
        public void Focus(IWebElement element)
        {
            
        }
       
        public void MoveTo(ActionArgs args)
        {
            var element = ElementFinder.Find(args.FindBy, args.ElementId);
            if (element != null)
            {
                var touchAction = new TouchAction(Driver);
                touchAction.MoveTo(element).Perform();
            }
        }        

        private bool IsUsedPort(int port)
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpEndPoints = properties.GetActiveTcpListeners();
            try
            {
                tcpEndPoints.Where(p => p.Port == port).First();
            }
            catch (Exception) { return false; }
            return true;
        }

        private void StartServer(int port)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "appium.bat";
            startInfo.Arguments = port.ToString();
            process.StartInfo = startInfo;
            process.Start();
            while (!IsUsedPort(port)) Thread.Sleep(1000);
        }

        private void StopServer(int port)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "appium-stop.bat";
            startInfo.Arguments = port.ToString();
            process.StartInfo = startInfo;
            process.Start();
            while (IsUsedPort(port)) Thread.Sleep(1000);
        }
        public void Back()
        {
            (Driver as AndroidDriver<AndroidElement>).PressKeyCode(AndroidKeyCode.Back);
        }

        protected override AppiumDriver<AndroidElement> CreateDriver( Uri inputUri)
        {
            return new AndroidDriver<AndroidElement>(inputUri, this.DesiredCapabilities, new TimeSpan(0,10,0));
        }
        
        protected override void CreateDesiredCapabilites(IStartAppArgs args)
        {
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformVersion, args.PlatformVersion);
            _desiredCapabilites.SetCapability(MobileCapabilityType.DeviceName, args.DeviceName);
            _desiredCapabilites.SetCapability(MobileCapabilityType.App, args.AppPackage);
            _desiredCapabilites.SetCapability(MobileCapabilityType.AutomationName, "uiautomator2");
            _desiredCapabilites.SetCapability(MobileCapabilityType.Udid, args.UDID);
            _desiredCapabilites.SetCapability(AndroidMobileCapabilityType.AppActivity, (args as IAndroidStartAppArgs).AppActivity);
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformName, "Android");
            _desiredCapabilites.SetCapability(MobileCapabilityType.NewCommandTimeout, 9999);
        }


    }
}
