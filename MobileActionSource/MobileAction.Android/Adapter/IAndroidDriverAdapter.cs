﻿using MobileAction.ActionArguments;
using OpenQA.Selenium.Appium.Android;

namespace MobileAction.Android
{
    public interface IAndroidDriverAdapter : IMobileDriverAdapter
    {
        void ConnectInternet();
        void DisconnectInternet();
        void MoveTo(ActionArgs args);
        void Back();
        void ReplaceValue(AndroidElement element, string value);
    }
}
