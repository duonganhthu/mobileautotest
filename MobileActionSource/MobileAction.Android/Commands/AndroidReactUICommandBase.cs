﻿using MobileAction.Base;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoTest.Data;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Remote;

namespace MobileAction.Android.Commands
{
    public abstract class AndroidReactUICommandBase<T> : ReactUICommandBase<T, AndroidElement> where T : CommandArgumentBase
    {
    }
}
