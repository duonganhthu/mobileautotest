﻿using MobileAction.Base;
using MobileAction.Commands;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Remote;
using System;

namespace MobileAction.Android.Commands
{
    public class AndroidStartAppArgument: StartAppArgument
    {
        [ArgumentAtt(Label = "AppActivity")]
        public string AppActivity { get; set; }
        [ArgumentAtt(Label = "AppWaitActivity")]
        public string AppWaitActivity { get; set; }
    }
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "StartApp")]
   public class StartApp : StartAppBase<AndroidStartAppArgument, AndroidElement>
    {
        protected override Type GetDriverType()
        {
            return typeof(AndroidDriver<AndroidElement>);
        }

        protected override DesiredCapabilities InitDesiredCapabilities(AndroidStartAppArgument args)
        {
            var _desiredCapabilites = new DesiredCapabilities();
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformVersion, args.PlatformVersion);
            _desiredCapabilites.SetCapability(MobileCapabilityType.DeviceName, args.DeviceName);
            _desiredCapabilites.SetCapability(MobileCapabilityType.App, args.AppPackage);
            _desiredCapabilites.SetCapability(MobileCapabilityType.AutomationName, "uiautomator2");
            _desiredCapabilites.SetCapability(MobileCapabilityType.Udid, args.UDID);
            _desiredCapabilites.SetCapability(MobileCapabilityType.FullReset, args.FullReset);
            _desiredCapabilites.SetCapability(AndroidMobileCapabilityType.AppActivity, args.AppActivity);
            _desiredCapabilites.SetCapability(AndroidMobileCapabilityType.AppWaitActivity, args.AppWaitActivity);
            _desiredCapabilites.SetCapability(MobileCapabilityType.PlatformName, "Android");
            _desiredCapabilites.SetCapability(MobileCapabilityType.NewCommandTimeout, 9999);
            return _desiredCapabilites;
        }
    }
}
