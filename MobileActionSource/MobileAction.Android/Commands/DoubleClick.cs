﻿using AutoTest.Data;
using MobileAction.Base;
using MobileAction.Commands;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Android.Commands
{
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "DoubleClick")]
    public class DoubleClick : AndroidReactUICommandBase<ClickArgument>
    {
        protected override object OnReactElement(ClickArgument argument, AndroidElement element, AppiumDriver<AndroidElement> driver, IStepInstance step)
        {
            var touchAction = new TouchAction(driver as IPerformsTouchActions);
            touchAction.Tap(element, null, null, 2).Perform();
            return null;
        }

        public override string Name
        {
            get
            {
                return "DoubleClick";
            }
        }
    }
}
