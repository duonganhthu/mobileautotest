﻿using AutoTest.Data;
using MobileAction.Base;
using MobileAction.Commands;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileAction.Android.Commands
{
    [ActionAtt(Group = DataStructure.ActionGroup.Command, Name = "ReplaceValue")]
    public class ReplaceValue : ReactUICommandBase<SetTextArgument, AndroidElement>
    {
        protected override object OnReactElement(SetTextArgument argument, AndroidElement element, AppiumDriver<AndroidElement> driver, IStepInstance step)
        {
            element.ReplaceValue(argument.Value);
            return null;
        }

        public override string Name
        {
            get
            {
                return "ReplaceValue";
            }
        }
    }
}
