﻿
using AutoTest.Data;
using OpenQA.Selenium.Appium.Android;

namespace MobileAction.Android
{
    public class AndroidAction : MobileAction<AndroidElement>
    {
        public AndroidAction()
        {
        }

        public AndroidAction(AndroidAction action) : base(action)
        {
        }

        public override string Name
        {
            get
            {
                return "Android Action";
            }
        }

        public override ActionBase Clone()
        {
            return new AndroidAction(this);
        }
    }
}
