﻿using Microsoft.Win32;
using mshtml;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AutoTest.WebAction.Spy
{
    [ComVisible(true),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
    Guid("FC4801A3-2BA9-11CF-A229-00AA003D7352")]
    public interface IObjectWithSite
    {
        [PreserveSig]
        int SetSite([MarshalAs(UnmanagedType.IUnknown)]object site);
        [PreserveSig]
        int GetSite(ref Guid guid, out IntPtr ppvSite);
    }

    [ComVisible(true),
    Guid("8a194578-81ea-4850-9911-13ba2d71efbd"),
    ClassInterface(ClassInterfaceType.None)]
    public class BHO : IObjectWithSite
    {
        #region Fields

        private const string _BHOKeyName = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";
        SHDocVw.WebBrowser _WebBrowser;
        bool _IsMouseDown;

        #endregion

        #region IObjectWithSite

        int IObjectWithSite.SetSite(object site)
        {
            if (site != null)
            {
                _WebBrowser = (SHDocVw.WebBrowser)site;
                _WebBrowser.DocumentComplete += webBrowser_DocumentComplete;
            }
            else
            {
                _WebBrowser.DocumentComplete -= webBrowser_DocumentComplete;
                _WebBrowser = null;
            }
            return 0;
        }

        int IObjectWithSite.GetSite(ref Guid guid, out IntPtr ppvSite)
        {
            IntPtr punk = Marshal.GetIUnknownForObject(_WebBrowser);
            int hr = Marshal.QueryInterface(punk, ref guid, out ppvSite);
            Marshal.Release(punk);

            return hr;
        }

        #endregion

        #region Events

        private void webBrowser_DocumentComplete(object pDisp, ref object URL)
        {
            HTMLDocument doc = (HTMLDocument)_WebBrowser.Document;
            mshtml.IHTMLStyleSheet css = (mshtml.IHTMLStyleSheet)doc.createStyleSheet("", 0);
            css.cssText = ".spy { border: solid 2px red !important; }";

            ((HTMLDocumentEvents2_Event)doc).onmouseover += document_onmouseover;
            ((HTMLDocumentEvents2_Event)doc).onmouseout += document_onmouseout;
            ((HTMLDocumentEvents2_Event)doc).onmousedown += document_onmousedown;

            foreach (var e in doc.all)
                if (e is HTMLElementEvents2_Event)
                    (e as HTMLElementEvents2_Event).onclick += element_onclick;
        }

        private bool element_onclick(IHTMLEventObj pEvtObj)
        {
            if (_IsMouseDown)
            {
                _IsMouseDown = false;
                pEvtObj.cancelBubble = true;
                pEvtObj.returnValue = false;
                return false;
            }
            return true;
        }

        private void document_onmousedown(IHTMLEventObj pEvtObj)
        {
            if (isSpying())
            {
                _IsMouseDown = true;
                Clipboard.SetText("AutoTestStop:" + getElementXPath(pEvtObj.srcElement as IHTMLDOMNode));

                removeSpyCssClass(pEvtObj);
                pEvtObj.cancelBubble = true;
                pEvtObj.returnValue = false;
            }
        }

        private void document_onmouseout(IHTMLEventObj pEvtObj)
        {
            if (isSpying())
            {
                removeSpyCssClass(pEvtObj);
            }
        }

        private void document_onmouseover(IHTMLEventObj pEvtObj)
        {
            if (isSpying())
            {
                IHTMLElement ele = pEvtObj.srcElement;
                ele.className += " spy ";
            }
        }

        private bool isSpying()
        {
            string var = Clipboard.GetText(TextDataFormat.Text);
            return string.Equals(var, "AutoTestStart", StringComparison.OrdinalIgnoreCase);
        }

        #endregion

        #region Methods

        [ComRegisterFunction]
        public static void RegisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(_BHOKeyName, true);
            if (registryKey == null)
                registryKey = Registry.LocalMachine.CreateSubKey(_BHOKeyName);

            string guid = type.GUID.ToString("B");
            RegistryKey ourKey = registryKey.OpenSubKey(guid);
            if (ourKey == null)
                ourKey = registryKey.CreateSubKey(guid);
            ourKey.SetValue("Alright", 1);
            registryKey.Close();
            ourKey.Close();
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(_BHOKeyName, true);
            string guid = type.GUID.ToString("B");
            if (registryKey != null)
                registryKey.DeleteSubKey(guid, false);
        }

        string getElementXPath(IHTMLDOMNode node)
        {
            IHTMLElement element = node as IHTMLElement;
            if (element != null && element.id != null)
                return string.Format("{0}{1}{2}", "Id", (char)1, element.id);
            else
                return string.Format("{0}{1}{2}", "XPath", (char)1, getElementTreeXPath(node));
        }

        string getElementTreeXPath(IHTMLDOMNode node)
        {
            List<string> paths = new List<string>();
            for (; node != null && node.nodeType == 1; node = node.parentNode)
            {
                var index = 0;
                for (var sibling = node.previousSibling; sibling != null; sibling = sibling.previousSibling)
                {
                    if (sibling.nodeType == 10)
                        continue;

                    if (sibling.nodeName == node.nodeName)
                        ++index;
                }

                var tagName = node.nodeName.ToLower();
                var pathIndex = (index > 0 ? "[" + (index + 1) + "]" : "");
                paths.Insert(0, tagName + pathIndex);
            }

            return paths.Count > 0 ? "/" + string.Join("/", paths) : null;
        }

        void removeSpyCssClass(IHTMLEventObj pEvtObj)
        {
            IHTMLElement ele = pEvtObj.srcElement;
            string cls = ele.className;
            if (!string.IsNullOrWhiteSpace(cls))
                ele.className = cls.Replace(" spy ", "");
        }

        #endregion
    }
}
