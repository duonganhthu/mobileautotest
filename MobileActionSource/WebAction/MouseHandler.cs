﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Threading;
using AutoTest.Data;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace WebAction
{
    internal static class MouseHandler
    {
        public static void Click(IWebElement webEle, IStepInstance step)
        {
            string script =
               "if (!me._fClick) {" +
                   "me._fClick = function () {" +
                       "me.removeEventListener('click', me._fClick);" +
                       "me._fClick = null;" +
                   "};" +
                   "me.addEventListener('click', me._fClick);" +
               "}";
            GeneralHandler.JavaScript(webEle, script);

            Action jsClick = () =>
            {
                try { GeneralHandler.JavaScript(webEle, "me.click();"); }
                catch { }
            };
            Func<bool> isClicked = () =>
            {
                try { return (bool)GeneralHandler.JavaScript(webEle, "return me._fClick == null"); }
                catch { return true; }
            };

            string tagName = (webEle.TagName ?? "").ToLower();
            if (tagName == "button" || tagName == "a" || tagName=="i")
                jsClick();

            if (!isClicked() || tagName == "input")
            {
                int x = (int)step.StepData[ParaInfo.XCoord.Name];
                int y = (int)step.StepData[ParaInfo.YCoord.Name];
                bool isIE = WebAction.CurrentWebDriver is InternetExplorerDriver;

                try
                {
                    if (!isIE || x != 0 || y != 0)
                    {
                        if (x == 0 && y == 0)
                        {
                            x = Math.Max(x, 3);
                            y = Math.Max(y, 3);
                        }
                        Actions action = new Actions(WebAction.CurrentWebDriver);
                        action.MoveToElement(webEle, x, y).Click().Release(webEle).Build().Perform();
                    }
                    else
                        webEle.Click();
                }
                catch { }
            }
        }

        public static void DoubleClick(IWebElement webEle)
        {
            Actions action = new Actions(WebAction.CurrentWebDriver);
            action.DoubleClick();
            action.Perform();
        }

        public static void MouseMove(IWebElement webEle)
        {
            Actions action = new Actions(WebAction.CurrentWebDriver);
            action.MoveToElement(webEle).MoveByOffset(5, 5).Build().Perform();
        }

        public static void MouseOver(IWebElement webEle)
        {
            Actions action = new Actions(WebAction.CurrentWebDriver);
            action.MoveToElement(webEle).MoveByOffset(5, 5).ClickAndHold().Release().Build().Perform();
        }

        public static void DragAndDrop(IWebElement webSourceEle, IMemory stepData)
        {
            Value method = stepData[ParaInfo.FindTargetBy.Name];
            FindBy findTargetBy = method.IsEmpty() ? FindBy.Id : (FindBy)Enum.Parse(typeof(FindBy), (string)method);
            string valueTargetFindToFind = (string)stepData[ParaInfo.ObjectTargetId.Name];
            int x = (int)stepData[ParaInfo.XCoord.Name];
            int y = (int)stepData[ParaInfo.YCoord.Name];


            By by = findTargetBy == FindBy.Id ? By.Id(valueTargetFindToFind) :
                   findTargetBy == FindBy.Name ? By.Name(valueTargetFindToFind) :
                   findTargetBy == FindBy.TagName ? By.TagName(valueTargetFindToFind) :
                   findTargetBy == FindBy.ClassName ? By.ClassName(valueTargetFindToFind) :
                   findTargetBy == FindBy.CssSelector ? By.CssSelector(valueTargetFindToFind) :
                   findTargetBy == FindBy.XPath ? By.XPath(valueTargetFindToFind) :
                   findTargetBy == FindBy.LinkText ? By.LinkText(valueTargetFindToFind) : By.PartialLinkText(valueTargetFindToFind);
            IWebElement webDestEle = WebAction.CurrentWebDriver.FindElement(by);

            Actions action = new Actions(WebAction.CurrentWebDriver);
            action.ClickAndHold(webSourceEle).MoveToElement(webDestEle, x, y).Release(webDestEle).Build().Perform();
        }
    }
}
