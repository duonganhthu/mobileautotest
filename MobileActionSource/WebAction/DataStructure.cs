﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAction
{
    internal enum IOTypes { Mouse, Keyboard, Set, Validate, Command }
    internal enum EventType
    {
        Click,
        DoubleClick,
        MouseDown,
        MouseUp,
        MouseMove,
        MouseOut,
        MouseOver,
        Dragdrop,
        KeyDown,
        KeyPress,
        KeyUp,
        SystemKeyDown,
        SystemKeyUp,
        SwitchToFrame,
        SwitchToWindows,
        SwitchToDefaultContent,
        TypeText,

        SetText,
        SetCheckBoxValue,
        SetDropDownItem,
        SetListBoxItems,

        GetText,
        CheckText,
        WaitText,

        WaitElementExisted,
        WaitElementNotExisted,
        WaitPage,
        LaunchApp,
        Navigate,
        Confirm,
        CloseWindow,
        JavaScript,
        Trigger,
        TestRail,
        API
    }
    internal enum FindBy { Id, Name, TagName, ClassName, CssSelector, XPath, LinkText, PartialLinkText }
    internal enum SystemKey { Ctrl, Shift, Atl, Enter, Escape }

    internal enum Status { Passed = 1, Blocked = 2, Untested = 3, Retest = 4, Failed = 5 }

    internal enum Method { Get, Post, Put }
    internal class ParaInfo
    {
        private ParaInfo(string label, string toolTip, IEnumerable<object> items = null)
        {
            this.Label = label;
            this.Name = label.Replace(" ", "");
            this.ToolTip = toolTip;

            if (items != null)
            {
                Items = new List<string>();
                foreach (var e in items)
                    Items.Add(e.ToString());
            }
        }

        private ParaInfo(string label, string toolTip, Type enumType)
            : this(label, toolTip, Enum.GetNames(enumType))
        {
        }
        public readonly string Name;
        public readonly string Label;
        public readonly string ToolTip;
        public readonly List<string> Items;

        #region Data

        public static readonly ParaInfo FindBy = new ParaInfo("Find By", "There are some ways to find object, please choose one", typeof(FindBy));
        public static readonly ParaInfo ObjectId = new ParaInfo("Object Identify", "String value to find object");
        public static readonly ParaInfo Condition = new ParaInfo("Condition", "By default application search for displayed elements. Should use this condition for special cases, ex: type='hidden' style='display:block");
        public static readonly ParaInfo XCoord = new ParaInfo("X", "Click at X-Coordination is a number");
        public static readonly ParaInfo YCoord = new ParaInfo("Y", "Click at Y-Coordination is a number");
        public static readonly ParaInfo Button = new ParaInfo("Button", "Mouse button is Left or Right", new List<string> { "Left", "Right" });
        public static readonly ParaInfo PageTitle = new ParaInfo("Page Title", "Wait until browser page title is equal this value");
        public static readonly ParaInfo StringValue = new ParaInfo("Value", "Value is a string");
        public static readonly ParaInfo ScriptValue = new ParaInfo("Script Content", "Using keywork \"me\" to access current element, ex: me.click();");
        public static readonly ParaInfo SystemKey = new ParaInfo("Key", "Special keyword", typeof(SystemKey));
        public static readonly ParaInfo Browser = new ParaInfo("Browser", "Browser Type", new List<string> { "Firefox", "IE", "Chrome", "Safari" });
        public static readonly ParaInfo EventTrigger = new ParaInfo("Event Trigger", "Event Trigger", new List<string> { "Click", "Double Click", "Hover", "Mouse Out", "Mouse Down", "Mouse Up", "Mouse Over", "Blur","Focus","Change","Submit","Select","Key Down", "Key Up", "Key Press", "Scroll","Resize" });
        public static readonly ParaInfo Width = new ParaInfo("Width", "Width of browser");
        public static readonly ParaInfo Height = new ParaInfo("Height", "Height of browser");
        public static readonly ParaInfo Event = new ParaInfo("Event", "Event for WebAction");
        public static readonly ParaInfo Url = new ParaInfo("Url", "Full url of appication");
        public static readonly ParaInfo RelativeUrl = new ParaInfo("Relative Url", "Relative url of appication");
        public static readonly ParaInfo FindTargetBy = new ParaInfo("Find Target By", "There are some ways to find object, please choose one", typeof(FindBy));
        public static readonly ParaInfo ObjectTargetId = new ParaInfo("Target Object Identify", "String value to find object");
        public static readonly ParaInfo IsChecked = new ParaInfo("Check", "Check is checked or unchecked", new List<string> { "Checked", "Un-Checked" });
        public static readonly ParaInfo TimeOut = new ParaInfo("Time Out", "The maximum time in SECOND for waiting");
        public static readonly ParaInfo UserName = new ParaInfo("Username", "Username");
        public static readonly ParaInfo PassWord = new ParaInfo("Password", "PassWord");
        public static readonly ParaInfo TestcaseId = new ParaInfo("Testcase Id", "Test Case Id");
        public static readonly ParaInfo TestRunID = new ParaInfo("Testrun Id", "Test Run Id");
        public static readonly ParaInfo SectionID = new ParaInfo("Section Id", "Section Id");
        public static readonly ParaInfo Status = new ParaInfo("Status", "Status", typeof(Status));
        public static readonly ParaInfo Comment = new ParaInfo("Comment", "Comment");
        public static readonly ParaInfo Method = new ParaInfo("Status", "Status", typeof(Method));
        public static readonly ParaInfo Data = new ParaInfo("Json Data", "Json Data");

        #endregion
    }

    #region WebCommand

    internal class WebCommand
    {
        internal WebCommand(IOTypes io, EventType ev, params ParaInfo[] paras)
        {
            IO = io;
            Event = ev;
            if (paras != null)
            {
                Paras = new List<ParaInfo>();
                foreach (ParaInfo e in paras)
                    Paras.Add(e);
            }
        }
        public IOTypes IO { get; set; }
        public EventType Event { get; set; }
        public readonly List<ParaInfo> Paras;

        public override string ToString()
        {
            return Event.ToString();
        }

        #region Data

        public static List<WebCommand> AllActions = new List<WebCommand>
        {
            new WebCommandClick(),
            new WebCommandDoubleClick(),
            new WebCommand(IOTypes.Mouse, EventType.MouseDown, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.Button, ParaInfo.XCoord, ParaInfo.YCoord ),
            new WebCommand(IOTypes.Mouse, EventType.MouseUp, ParaInfo.FindBy,ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.Button, ParaInfo.XCoord, ParaInfo.YCoord ),
            new WebCommand(IOTypes.Mouse, EventType.MouseMove, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition ),
            new WebCommand(IOTypes.Mouse, EventType.MouseOut, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition),
            new WebCommand(IOTypes.Mouse, EventType.MouseOver, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition),
            new WebCommand(IOTypes.Mouse, EventType.Dragdrop, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.FindTargetBy, ParaInfo.ObjectTargetId ),

            new WebCommand(IOTypes.Keyboard, EventType.SystemKeyDown, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.SystemKey ),
            new WebCommand(IOTypes.Keyboard, EventType.SystemKeyUp, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.SystemKey),
            new WebCommand(IOTypes.Keyboard, EventType.TypeText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue ),
            new WebCommand(IOTypes.Keyboard, EventType.SwitchToFrame, ParaInfo.FindBy, ParaInfo.ObjectId),
            new WebCommand(IOTypes.Keyboard, EventType.SwitchToWindows, ParaInfo.PageTitle,ParaInfo.TimeOut),
            new WebCommand(IOTypes.Keyboard, EventType.SwitchToDefaultContent),

            new WebCommand(IOTypes.Set, EventType.SetText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Set, EventType.SetCheckBoxValue, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.IsChecked),
            new WebCommand(IOTypes.Set, EventType.SetDropDownItem, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue),
            new WebCommand(IOTypes.Set, EventType.SetListBoxItems, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue),

            new WebCommand(IOTypes.Validate, EventType.GetText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition),
            new WebCommand(IOTypes.Validate, EventType.CheckText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue),
            new WebCommand(IOTypes.Validate, EventType.WaitText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue, ParaInfo.TimeOut),

            new WebCommandLaunchApp(),
            new WebCommand(IOTypes.Command, EventType.WaitPage, ParaInfo.PageTitle, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Command, EventType.WaitElementExisted, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Command, EventType.WaitElementNotExisted, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Command, EventType.Navigate, ParaInfo.Url, ParaInfo.PageTitle, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Command, EventType.Confirm, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.TimeOut),
            new WebCommand(IOTypes.Command, EventType.CloseWindow),
            new WebCommand(IOTypes.Command, EventType.JavaScript, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.ScriptValue),
            new WebCommand(IOTypes.Command, EventType.Trigger, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.EventTrigger),
            new WebCommand(IOTypes.Command, EventType.TestRail, ParaInfo.UserName, ParaInfo.PassWord, ParaInfo.TestRunID, ParaInfo.TestcaseId, ParaInfo.SectionID, ParaInfo.Status, ParaInfo.Comment),
            new WebCommand(IOTypes.Command, EventType.API, ParaInfo.UserName, ParaInfo.PassWord, ParaInfo.Url, ParaInfo.Method, ParaInfo.Data),
        };

        #endregion
    }

    #endregion

}
