﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using AutoTest.Data;
using System.Threading;
using System.Collections.Specialized;
using OpenQA.Selenium.Remote;
using System.Windows.Automation;
using System.Text.RegularExpressions;
using System.Drawing;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.Script.Serialization;

namespace WebAction
{
    internal static class GeneralHandler
    {

        public static void LauchApp(IStepInstance step, IMemory stepData)
        {
            string browser = (string)stepData[ParaInfo.Browser.Name];

            int width = (int)stepData[ParaInfo.Width.Name];
            int height = (int)stepData[ParaInfo.Height.Name];
            //int width = 0;
            //if (int.TryParse(stepData[ParaInfo.Width.Name].ToString(), out width))
            //    width = (int)stepData[ParaInfo.Width.Name];
            //else
            //    System.Windows.MessageBox.Show("Please insert correct value for width");


            //int height = 0;
            //if (int.TryParse(stepData[ParaInfo.Height.Name].ToString(), out width))
            //    width = (int)stepData[ParaInfo.Height.Name];
            //else
            //    System.Windows.MessageBox.Show("Please insert correct value for height");

            step.Log(ParaInfo.Browser.Name, " = ", browser); 

            string url = (string)stepData[ParaInfo.Url.Name];
            step.Log(ParaInfo.Url.Name, " = ", url);

            AutomationElementCollection windowAppCollection1 = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);

            string windowName = "";

            WebAction.KillWebDriverService();

            try
            {
                ReadOnlyCollection<string> handles = WebAction.CurrentWebDriver.WindowHandles;
                foreach (string handle in handles)
                {
                    WebAction.CurrentWebDriver.SwitchTo().Window(handle).Close();
                }
            }
            catch (Exception) { }


            switch (browser)
            {
                case "IE":
                    WebAction.CurrentDriverService = InternetExplorerDriverService.CreateDefaultService();
                    WebAction.CurrentDriverService.HideCommandPromptWindow = true;
                    InternetExplorerOptions options = new InternetExplorerOptions();
                    options.RequireWindowFocus = true;
                    options.EnableNativeEvents = false;
                    options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    WebAction.CurrentWebDriver = new InternetExplorerDriver((InternetExplorerDriverService)WebAction.CurrentDriverService, options);
                    windowName = "Internet Explorer";
                    break;
                case "Chrome":
                    WebAction.CurrentDriverService = ChromeDriverService.CreateDefaultService();
                    WebAction.CurrentDriverService.HideCommandPromptWindow = true;
                   
                  //  optionsChrome.AddArgument(@"user-data-dir=C:\Users\quocnna\AppData\Local\Google\Chrome\User Data\\Default\\");
                   
                 //   string path = Environment.CurrentDirectory;
                  // DirectoryInfo di = new DirectoryInfo(path);
                   // string pathFile = di.FullName + "\\PostManForChrome.crx";
                    string pathFile = Path.GetFullPath("PostManForChrome.crx");
               //    string pathChrome = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                   
                 //   optionsChrome.BinaryLocation = pathChrome;
                    
                    //DesiredCapabilities capabilities = DesiredCapabilities.Chrome();
                    //capabilities.SetCapability("platform", "Windows 8" );
                    //capabilities.SetCapability("version", "10");
                    //capabilities.SetCapability("screen-resolution", "1280x1024");
                    ChromeOptions optionsChrome = new ChromeOptions();
                    optionsChrome.AddExtension(pathFile);
                //    optionsChrome.AddArgument(@"--user-data-dir=C:\Users\quocnna\AppData\Local\Google\Chrome\User Data\Default");
                   // optionsChrome.AddAdditionalCapability(ChromeOptions.Capability, new object[] { "disable-default-apps" });
                    //capabilities.SetCapability(ChromeOptions.Capability, optionsChrome);
                    WebAction.CurrentWebDriver = new ChromeDriver((ChromeDriverService)WebAction.CurrentDriverService, optionsChrome);
   
                    
                    windowName = "Chrome";
                    break;
                case "Safari":
                    WebAction.CurrentWebDriver = new SafariDriver();
                    windowName = "Safari";
                    break;
                default:
                    WebAction.CurrentWebDriver = new FirefoxDriver();
                    windowName = "Firefox";
                    break;
            }

            if (width > 0 && height > 0)
                WebAction.CurrentWebDriver.Manage().Window.Size = new Size(width, height);
            else
                WebAction.CurrentWebDriver.Manage().Window.Maximize();
            WebAction.CurrentWebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(500));
            WebAction.CurrentWebDriver.Navigate().GoToUrl(url);

            #region Window Automation

            AutomationElementCollection windowAppCollection2 = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);
            bool stop = false;
            AutomationElement ele = null;

            for (int i2 = 0; i2 < windowAppCollection2.Count && !stop; i2++)
            {
                ele = windowAppCollection2[i2];
                try
                {
                    if ((ele.GetCurrentPropertyValue(AutomationElementIdentifiers.NameProperty) as string).Contains(windowName))
                        for (int i1 = 0; i1 < windowAppCollection1.Count && !stop; i1++)
                            stop = ele != windowAppCollection1[i1];
                }
                catch { }
            }

            if (ele != null)
                TestModel.MainWindowHandle = new IntPtr(ele.Current.NativeWindowHandle);

            #endregion
        }
        public static bool WaitElementNotExisted(WebDriverWait wait, IStepInstance step, By findBy)
        {
            bool result = true;
            wait.Until<bool>((driver) =>
            {
                try
                {
                    WaitElementExisted(step, findBy);
                    return result = false;
                }
                catch
                {
                    return result = true;
                }
            });
            return result;
        }
        public static IWebElement WaitElementExisted(IStepInstance step, By findBy)
        {
            step.Log("WaitElementExisted...");
            IWebElement webEle = WebAction.CurrentWebDriver.FindElement(findBy);
            bool match = webEle != null;
            if (match)
            {
                JavaScript(webEle, "me.style && (me.style.border = '2px solid blue');");

                Dictionary<string, string> attributes = attributeParser((step.StepData[ParaInfo.Condition.Name] ?? "").ToString());
                if (attributes.Count == 0)
                {
                    step.Log("Displayed: ", webEle.Displayed);
                    match = match && webEle.Displayed;
                }
                else
                {
                    step.Log("Check Condition:");
                    foreach (var attr in attributes)
                        if (match)
                        {
                            if (string.Equals(attr.Key, "text", StringComparison.OrdinalIgnoreCase))
                            {
                                string value = webEle.Text;
                                match = match && string.Equals(value, attr.Value, StringComparison.OrdinalIgnoreCase);
                                step.Log("Text[", attr.Value, "==", value, "] =>", match);
                            }
                            else if (string.Equals(attr.Key, "style", StringComparison.OrdinalIgnoreCase))
                            {
                                Dictionary<string, string> styles = styleParser(attr.Value);
                                foreach (var style in styles)
                                {
                                    string value = webEle.GetCssValue(style.Key);
                                    match = match && string.Equals(value, style.Value, StringComparison.OrdinalIgnoreCase);
                                    step.Log(style.Key, "[", style.Value, "==", value, "] =>", match);
                                }
                            }
                            else
                            {
                                string value = webEle.GetAttribute(attr.Key);
                                match = match && string.Equals(value, attr.Value, StringComparison.OrdinalIgnoreCase);
                                step.Log(attr.Key, "[", attr.Value, "==", value, "] =>", match);
                            }
                        }
                        else
                            break;
                }
            }

            webEle = match ? webEle : null;
            step.Log(webEle == null ? "WaitElementExisted FAIL" : "WaitElementExisted Success");
            return webEle;
        }
        public static bool WaitPage(WebDriverWait wait, IMemory stepData)
        {
            string title = (string)stepData[ParaInfo.PageTitle.Name] ?? "";
            bool result = true;
            if (!string.IsNullOrEmpty(title))
                wait.Until<bool>(driver =>
                {
                    result = string.Equals(WebAction.CurrentWebDriver.Title, title, StringComparison.OrdinalIgnoreCase);
                    if (!result)
                        Thread.Sleep(500);
                    return result;
                });
            return result;
        }

        private static Dictionary<string, string> attributeParser(string strValue)
        {
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            StringBuilder sb = new StringBuilder();
            string key = null;

            for (int i = 0; i < strValue.Length; i++)
            {
            Start:
                char ch = strValue[i];
                if (ch == ' ' || ch == '=')
                {
                    if (sb.Length == 0)
                        continue;

                    key = sb.ToString();

                    attributes[key] = null;
                    if (ch == ' ')
                    {
                        while (++i < strValue.Length)
                        {
                            ch = strValue[i];
                            if (ch == ' ')
                                continue;
                            else if (ch == '=')
                                break;
                            else //<input type='checkbox' CHECKED />
                            {
                                sb.Clear();
                                goto Start;
                            }
                        }
                    }

                    if (ch == '=')
                    {
                        while (++i < strValue.Length)
                        {
                            ch = strValue[i];
                            if (ch == ' ')
                                continue;
                            else if (ch == '\'' || ch == '"')
                            {
                                int j = i < strValue.Length - 1 ? strValue.IndexOf(ch, i + 1) : -1;
                                if (j > 0)
                                {
                                    string v = strValue.Substring(i + 1, j - i - 1).Trim();
                                    attributes[key] = v;
                                    i = j + 1;
                                    if (i >= strValue.Length)
                                        return attributes;
                                }
                                sb.Clear();
                                goto Start;
                            }
                            else //<input type=CHECKBOX/>
                            {
                                sb.Clear();
                                do
                                {
                                    ch = strValue[i];
                                    if (ch == ' ')
                                    {
                                        if (sb.Length == 0)
                                            continue;
                                        else
                                            break;
                                    }
                                    else
                                        sb.Append(ch);
                                }
                                while (++i < strValue.Length);
                                attributes[key] = sb.ToString();
                                sb.Clear();
                                goto Start;
                            }
                        }
                    }
                }
                else
                    sb.Append(ch);
            }

            return attributes;
        }
        private static Dictionary<string, string> styleParser(string strValue)
        {
            Dictionary<string, string> styles = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(strValue))
                return styles;

            StringBuilder sb = new StringBuilder();
            string key = null;

            Action CreateElement = () =>
            {
                if (key == null)
                {
                    key = sb.ToString().Trim();
                    styles[key] = null;
                }
                else
                    styles[key] = sb.ToString().Trim();
            };

            for (int i = 0; i < strValue.Length; i++)
            {
                char ch = strValue[i];
                switch (ch)
                {
                    case ':':
                        key = sb.ToString().Trim();
                        styles[key] = null;
                        sb.Clear();
                        break;
                    case ';':
                        CreateElement();
                        key = null;
                        sb.Clear();
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            if (sb.Length > 0)
                CreateElement();

            return styles;
        }

        public static string GetText(IWebElement webEle)
        {
            return webEle.Text;
        }
        public static bool CheckText(IWebElement webEle, IMemory stepData)
        {
            string val1 = (webEle.Text ?? "").ToLower();
            string val2 = ((string)stepData[ParaInfo.StringValue.Name] ?? "").ToString().ToLower();
            return val1 == val2;
        }

        public static bool SetText(IWebElement webEle, IMemory stepData)
        {
            string value = (string)stepData[ParaInfo.StringValue.Name] ?? "";
            if (webEle.TagName == "body" && WebAction.CurrentWebDriver is InternetExplorerDriver)
            {
                JavaScript(webEle, "me.focus();me.innerHTML='" + value + "';");
                return true;
            }

            webEle.Clear();
            try
            {
                if (!String.IsNullOrEmpty(JavaScript(webEle, "return me.value;").ToString().Trim()))
                {
                    JavaScript(webEle, "me.value='';").ToString().Trim();
                }
            }
            catch { }

            webEle.SendKeys(value);
            trigger(webEle, "change");

            string checkVal = string.Empty;
            if (webEle.TagName != "body")
            {
                checkVal = (JavaScript(webEle, "return me.value;") ?? "").ToString().Trim();
                return checkVal.Equals(value.Trim(), StringComparison.OrdinalIgnoreCase);
            }
            return true;
        }

        public static bool Navigate(WebDriverWait wait, IMemory stepData)
        {
            string url = (string)stepData[ParaInfo.Url.Name];

            if (WebAction.CurrentWebDriver.Url != url)
                WebAction.CurrentWebDriver.Navigate().GoToUrl(url);
            else
                WebAction.CurrentWebDriver.Navigate().Refresh();

            return WaitPage(wait, stepData);
        }

        public static void Close()
        {
            WebAction.CurrentWebDriver.Close();
            ReadOnlyCollection<string> handles = WebAction.CurrentWebDriver.WindowHandles;
            if (handles.Count > 0)
            {
                WebAction.CurrentWebDriver.SwitchTo().Window(handles[0]);
            }
        }

        public static bool SetDropDownItem(IWebElement webEle, IMemory stepData)
        {
            string[] values = new string[] { (string)stepData[ParaInfo.StringValue.Name] };

            if (WebAction.CurrentWebDriver is InternetExplorerDriver)
                return setSelectedItems(webEle, values);

            return setSelectedItems(webEle, values) && checkSelectedItems(webEle, values);
        }
        public static bool SetListBoxItems(IWebElement webEle, IMemory stepData)
        {
            string[] values = (((string)stepData[ParaInfo.StringValue.Name]) ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            return setSelectedItems(webEle, values) && checkSelectedItems(webEle, values);
        }
        private static bool setSelectedItems(IWebElement webEle, string[] values)
        {
            //while ("?".Equals(JavaScript(webEle, "return me.value;")))
            //{
            //    Thread.Sleep(100);
            //    return false;
            //}

            StringBuilder sb = new StringBuilder();
            foreach (string v in values)
                sb.Append((sb.Length > 0 ? "," : "") + "'" + v + "'");

            string script =
                "var hash = {};" +
                "var texts = [" + sb.ToString() + "];" +
                "for (var i in texts)" +
                    "hash[texts[i]] = true;" +
                "me.selectedIndex = -1;" +
                "for (var i=0; i < me.options.length;i++) {" +
                    "var txt = me.options[i].text;" +
                    "if ((me.options[i].selected = hash[txt]) && !me.multiple)" +
                        "me.selectedIndex = i;" +
                "}";
            JavaScript(webEle, script);

            trigger(webEle, "change");

            return true;
        }
        private static bool checkSelectedItems(IWebElement webEle, string[] values)
        {
            object[] checkValues = getSelectedItems(webEle);
            if (checkValues == null || values == null || checkValues.Length != values.Length)
                return false;

            for (int i = 0; i < values.Length; i++)
                if (!values[i].Equals(checkValues[i]))
                    return false;

            return true;
        }
        private static string[] getSelectedItems(IWebElement webEle)
        {
            string script =
                "var eles = [];" +
                "if (me.selectedOptions)" +
                    "for (var i = 0; i < me.selectedOptions.length; i++)" +
                        "eles.push(me.selectedOptions[i].text);" +
                "return eles;";

            List<string> result = new List<string>();
            IEnumerable<object> values = JavaScript(webEle, script) as IEnumerable<object>;
            if (values != null)
                foreach (object o in values)
                    result.Add(o.ToString());
            return result.ToArray();
        }

        public static void Confirm(IWebElement webEle)
        {
            webEle.Click();
            IAlert alert = WebAction.CurrentWebDriver.SwitchTo().Alert();
            alert.Accept();
            WebDriverWait wait = new WebDriverWait(WebAction.CurrentWebDriver, TimeSpan.FromSeconds(10));
        }

        public static void Open(IStepInstance step, IMemory stepData)
        {
            string relativeUrl = (string)stepData[ParaInfo.RelativeUrl.Name] ?? "";
            step.Log(ParaInfo.RelativeUrl.Name, " = ", relativeUrl);

            string url = WebAction.CurrentWebDriver.Url;
            int i = url.IndexOf('/', 7);
            url = i < 0 ? url : url.Substring(0, i);
            string st = relativeUrl.First() == '/' ? "" : "/";
            url = url + st + relativeUrl;

            step.Log("Full Url = ", url);

            WebAction.CurrentWebDriver.Navigate().GoToUrl(url);
        }

        public static void SetCheckBoxValue(IWebElement webEle, IMemory stepData)
        {
            bool isCheck = (string)stepData[ParaInfo.IsChecked.Name] == "Checked";

            if ((isCheck && !webEle.Selected) || (!isCheck && webEle.Selected))
                JavaScript(webEle, "me.click();");
        }

        public static object JavaScript(IWebElement webEle, IMemory stepData)
        {
            return JavaScript(webEle, (string)stepData[ParaInfo.ScriptValue.Name] + ";");
        }
        public static object JavaScript(IWebElement webEle, string script)
        {
            script = "var me = arguments[0];" + script;
            return (WebAction.CurrentWebDriver as IJavaScriptExecutor).ExecuteScript(script, webEle);
        }

        public static void CloseWindow()
        {
            ReadOnlyCollection<string> handles = WebAction.CurrentWebDriver.WindowHandles;
            WebAction.CurrentWebDriver.Close();
            if (handles.Count > 0)
            {
                WebAction.CurrentWebDriver.SwitchTo().Window(handles[0]);
            }
        }

        public static void Focus(IWebElement webEle)
        {
            if (webEle != null)
                try
                {
                    if (string.Equals("input", webEle.TagName, StringComparison.OrdinalIgnoreCase))
                        webEle.SendKeys("");
                    else
                        new Actions(WebAction.CurrentWebDriver).MoveToElement(webEle).Perform();
                }
                catch { }
        }

        public static void Trigger(IWebElement webEle, IMemory stepData)
        {
            trigger(webEle, (string)stepData[ParaInfo.EventTrigger.Name]);
        }
        private static void trigger(IWebElement webEle, string eventName)
        {
            try
            {
                eventName = (eventName ?? "").Replace(" ", "").ToLower();
                switch (eventName)
                {
                    case "doubleclick":
                        JavaScript(webEle, "$(me).trigger('dblclick');");
                        break;
                    default:
                        JavaScript(webEle, "$(me).trigger('" + eventName + "');");
                        break;
                }
            }
            catch
            {
            }
        }


        public static int TestRail(IMemory stepData)
        {
            string testCaseId = (string)stepData[ParaInfo.TestcaseId.Name];
            HttpWebRequest request;
            HttpWebResponse response;
            Exception ex = null;
            int result = 0;

            string auth = Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(
                    String.Format(
                        "{0}:{1}",
                        (string)stepData[ParaInfo.UserName.Name],
                        (string)stepData[ParaInfo.PassWord.Name]
                    )
                )
            );

            if (testCaseId != null)
            {
                request = (HttpWebRequest)WebRequest.Create("https://neogov.testrail.com/index.php?/api/v2/get_case/" + testCaseId);
                request.ContentType = "application/json";
                request.Method = "GET";
                request.Headers.Add("Authorization", "Basic " + auth);
                response = null;
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException e)
                {
                    if (e.Response == null)
                    {
                        throw;
                    }

                    response = (HttpWebResponse)e.Response;
                    ex = e;
                }
                result = (int)response.StatusCode;
            }

            if (result == 200) //search found test case
            {
                request = (HttpWebRequest)WebRequest.Create("https://neogov.testrail.com/index.php?/api/v2/add_result_for_case/"+(string)stepData[ParaInfo.TestRunID.Name]+"/"+(string)stepData[ParaInfo.TestcaseId.Name]);
                request.ContentType = "application/json";
                request.Method = "POST";
                request.Headers.Add("Authorization", "Basic " + auth);

                Value method = stepData[ParaInfo.Status.Name];
                Status status = method.IsEmpty() ? Status.Untested : (Status)Enum.Parse(typeof(Status), (string)method);

                var data = new Dictionary<string, object>
                {
	                { "status_id", (int)Enum.Parse(status.GetType(), status.ToString())},
	                { "comment", (string)stepData[ParaInfo.Comment.Name]}
                };

                if (data != null)
                {
                    var ser = new JavaScriptSerializer();

                    byte[] block = Encoding.UTF8.GetBytes(ser.Serialize(data));
                    request.GetRequestStream().Write(block, 0, block.Length);
                }

                response = null;
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException e)
                {
                    if (e.Response == null)
                    {
                        throw;
                    }

                    response = (HttpWebResponse)e.Response;
                    ex = e;
                }

                return (int)stepData[ParaInfo.TestcaseId.Name];
            }
            else //create new case
            {
                string testCase = (string)stepData[ParaInfo.Comment.Name];
                string text = "";
                if (testCase != null)
                {
                    request = (HttpWebRequest)WebRequest.Create("https://neogov.testrail.com/index.php?/api/v2/add_case/" + (string)stepData[ParaInfo.SectionID.Name]);
                    request.ContentType = "application/json";
                    request.Method = "POST";
                    request.Headers.Add("Authorization", "Basic " + auth);

                    var data = new Dictionary<string, object>
                    {
	                    { "title", testCase},
                        {"custom_tc_scope", 3},
                        {"custom_tc_category", 1},
                        {"custom_tc_automated", 1}
                    };


                    if (data != null)
                    {
                        var ser = new JavaScriptSerializer();

                        byte[] block = Encoding.UTF8.GetBytes(ser.Serialize(data));
                        request.GetRequestStream().Write(block, 0, block.Length);
                    }

                    response = null;
                    try
                    {
                        response = (HttpWebResponse)request.GetResponse();
                    }
                    catch (WebException e)
                    {
                        if (e.Response == null)
                        {
                            throw;
                        }

                        response = (HttpWebResponse)e.Response;
                        ex = e;
                    }

                    // Read the response body, if any, and deserialize it from JSON.
                    
                    if (response != null)
                    {
                        var reader = new StreamReader(
                            response.GetResponseStream(),
                            Encoding.UTF8
                        );

                        using (reader)
                        {
                            text = reader.ReadToEnd();
                        }
                    }
                }
                return Convert.ToInt32(text.Substring(6, text.IndexOf(",\"title") - 6));
            }

        }

        public static string API(IMemory stepData)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create((string)stepData[ParaInfo.Url.Name]);
            request.ContentType = "application/json";
            request.Method = (string)stepData[ParaInfo.Method.Name];

            string auth = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(
                    String.Format(
                        "{0}:{1}",
                        (string)stepData[ParaInfo.UserName.Name],
                        (string)stepData[ParaInfo.PassWord.Name]
                    )
                )
            );


            request.Headers.Add("Authorization", "Basic " + auth);

            if (request.Method != "GET")
            {

                string data = (string)stepData[ParaInfo.Data.Name];

                if (data != null)
                {
                    byte[] block = Encoding.UTF8.GetBytes(data);
                    request.GetRequestStream().Write(block, 0, block.Length);
                }
            }

            Exception ex = null;
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw;
                }

                response = (HttpWebResponse)e.Response;
                ex = e;
            }

            // Read the response body, if any, and deserialize it from JSON.
            string text = "";
            if (response != null)
            {
                var reader = new StreamReader(
                    response.GetResponseStream(),
                    Encoding.UTF8
                );

                using (reader)
                {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }
    }
}
