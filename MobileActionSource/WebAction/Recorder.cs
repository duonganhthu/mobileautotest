﻿using AutoTest.Data;
using mshtml;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

namespace AutoTest.WebAction
{
    public class Recorder
    {
        private enum Events { Click, Blur, Change }

        #region Fields

        private InternetExplorer _IE;
        private ICollection<TestStep> _Steps;
        private IHTMLElement _LastElement;
        private Events _LastEvent;

        #endregion

        #region Methods

        public void Start(ICollection<TestStep> steps)
        {
            _Steps = steps;

            if (_IE == null)
            {
                _IE = new InternetExplorer();
                _IE.Visible = true;

                ((WebBrowser_V1)_IE).BeforeNavigate += browser_onBeforeNavigate;
                _IE.DocumentComplete += document_onDocumentComplete;

                _IE.OnQuit += browser_onQuit;
            }
        }

        void browser_onBeforeNavigate(string URL, int Flags, string TargetFrameName, ref object PostData, string Headers, ref bool Cancel)
        {
            if (Flags == 0)
            {
                TestStep step = _Steps.Count == 0 ?
                    new WebCommandLaunchApp().CreateTestStep("Launch application " + URL, "IE", URL) :
                    new WebCommandNavigate().CreateTestStep("Navigate " + URL, URL); ;
                _Steps.Add(step);
            }
        }
        void browser_onQuit()
        {
            if (_IE != null)
            {
                ((WebBrowser_V1)_IE).BeforeNavigate -= browser_onBeforeNavigate;
                _IE.DocumentComplete -= document_onDocumentComplete;
                _IE = null;
            }
        }

        void document_onDocumentComplete(object pDisp, ref object URL)
        {
            try
            {
                HTMLDocument doc = (pDisp as WebBrowser_V1).Document;
                ((HTMLDocumentEvents2_Event)doc).onclick += (pEvtObj) => { return executeEvent(pEvtObj, Events.Click); };

                foreach (var e in doc.all)
                    if (e is HTMLInputTextElementEvents2_Event && isInputText(e as IHTMLElement))
                        (e as HTMLInputTextElementEvents2_Event).onblur += (pEvtObj) => { executeEvent(pEvtObj, Events.Blur); };
                    else if (e is HTMLSelectElementEvents2_Event)
                        (e as HTMLSelectElementEvents2_Event).onchange += (pEvtObj) => { executeEvent(pEvtObj, Events.Change); };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "AutoTest-Error");
            }
        }

        private bool executeEvent(IHTMLEventObj pEvtObj, Events ev)
        {

            IHTMLElement ele = pEvtObj.srcElement;
            if (_LastElement != ele || _LastEvent != ev)
                try
                {
                    string[] info = getElementXPath(ele as IHTMLDOMNode);
                    string findBy = info[0];
                    string xPath = info[1];
                    string title = "";
                    TestStep step = null;

                    switch (ev)
                    {
                        case Events.Click:
                            if (ele.tagName.ToLower() != "select")
                            {
                                title = string.Format("Click on {0}", getName(ele));
                                step = new WebCommandClick().CreateTestStep(title, findBy, xPath);
                            }
                            break;
                        case Events.Blur:
                            title = string.Format("Set value {0}: {1}", getName(ele), ele.getAttribute("value"));
                            step = new WebCommandSetText().CreateTestStep(title, findBy, xPath, null, ele.getAttribute("value"));
                            break;
                        case Events.Change:
                            DispHTMLSelectElement select = ele as DispHTMLSelectElement;
                            string selectedItems = ((DispHTMLOptionElement)select.options[select.selectedIndex]).innerText;
                            if (select.multiple)
                            {
                                StringBuilder sb = new StringBuilder();
                                foreach (DispHTMLOptionElement item in select.options)
                                    if (item.selected)
                                        sb.Append(", " + item.innerText);
                                selectedItems = (sb.Length > 0 ? sb.Remove(0, 2) : sb).ToString();
                            }

                            title = string.Format("Select {0}", getName(ele));
                            step = select.multiple ?
                                new WebCommandSetListBoxItems().CreateTestStep(title, findBy, xPath, null, selectedItems) :
                                new WebCommandSetDropDownItem().CreateTestStep(title, findBy, xPath, null, selectedItems);
                            break;
                    }

                    _LastElement = ele;
                    _LastEvent = ev;
                    if (step != null)
                        _Steps.Add(step);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "AutoTest-Error");
                }

            return true;
        }

        private string getName(IHTMLElement ele)
        {
            string type = ele.tagName.ToLower();
            string name = name = ele.id ?? ele.innerText ?? getElementXPath((IHTMLDOMNode)ele).Last();
            switch (type)
            {
                case "input":
                    type = ele.getAttribute("type");
                    name = type == "button" ? ele.getAttribute("value") : (ele.getAttribute("id") ?? ele.getAttribute("name"));
                    break;
                case "a":
                    type = "link";
                    break;
            }

            return string.Format("{0} [{1}]", type, name);

        }
        string[] getElementXPath(IHTMLDOMNode node)
        {
            IHTMLElement element = node as IHTMLElement;
            if (element != null && element.id != null)
                return new string[] { "Id", element.id };
            else
                return new string[] { "XPath", getElementTreeXPath(node) };
        }
        string getElementTreeXPath(IHTMLDOMNode node)
        {
            List<string> paths = new List<string>();
            for (; node != null && node.nodeType == 1; node = node.parentNode)
            {
                var index = 0;
                for (var sibling = node.previousSibling; sibling != null; sibling = sibling.previousSibling)
                {
                    if (sibling.nodeType == 10)
                        continue;

                    if (sibling.nodeName == node.nodeName)
                        ++index;
                }

                var tagName = node.nodeName.ToLower();
                var pathIndex = (index > 0 ? "[" + (index + 1) + "]" : "");
                paths.Insert(0, tagName + pathIndex);
            }

            return paths.Count > 0 ? "/" + string.Join("/", paths) : null;
        }

        private bool isInputText(IHTMLElement ele)
        {
            string tagName = ele.tagName.ToLower();
            string type = (ele.getAttribute("type") as string ?? "").ToLower();
            return (tagName == "input" & (type == "text" || type == "password")) || tagName == "textarea";
        }

        #endregion
    }
}
