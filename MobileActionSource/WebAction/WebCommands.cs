﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAction
{
    internal class WebCommandClick : WebCommand
    {
        public WebCommandClick()
            : base(IOTypes.Mouse, EventType.Click, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.XCoord, ParaInfo.YCoord)
        { }
    }
    internal class WebCommandDoubleClick : WebCommandClick
    {
        public WebCommandDoubleClick()
            : base()
        {
            Event = EventType.DoubleClick;
        }
    }

    internal class WebCommandMouseDown : WebCommand
    {
        public WebCommandMouseDown()
            : base(IOTypes.Mouse, EventType.MouseDown, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.Button, ParaInfo.XCoord, ParaInfo.YCoord)
        {
        }
    }

    internal class WebCommandMouseUp : WebCommandMouseDown
    {
        public WebCommandMouseUp()
            : base()
        {
            Event = EventType.MouseUp;
        }
    }
    internal class WebCommandMouseMove : WebCommand
    {
        public WebCommandMouseMove()
            : base(IOTypes.Mouse, EventType.MouseMove, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition)
        {
        }
    }
    internal class WebCommandMouseOut : WebCommandMouseMove
    {
        public WebCommandMouseOut()
            : base()
        {
            Event = EventType.MouseOut;
        }
    }
    internal class WebCommandMouseOver : WebCommandMouseMove
    {
        public WebCommandMouseOver()
            : base()
        {
            Event = EventType.MouseOver;
        }
    }

    internal class WebCommandDragdrop : WebCommand
    {
        public WebCommandDragdrop()
            : base(IOTypes.Mouse, EventType.Dragdrop, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.FindTargetBy, ParaInfo.ObjectTargetId)
        {
        }
    }

    internal class WebCommandSystemKeyDown : WebCommand
    {
        public WebCommandSystemKeyDown()
            : base(IOTypes.Keyboard, EventType.SystemKeyDown, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.SystemKey)
        {
        }
    }
    internal class WebCommandSystemKeyUp : WebCommandSystemKeyDown
    {
        public WebCommandSystemKeyUp()
            : base()
        {
            Event = EventType.SystemKeyUp;
        }
    }
    internal class WebCommandTypeText : WebCommand
    {
        public WebCommandTypeText()
            : base(IOTypes.Keyboard, EventType.TypeText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue)
        {
        }
    }

    internal class WebCommandSwitchTo : WebCommand
    {
        public WebCommandSwitchTo()
            : base(IOTypes.Keyboard, EventType.SwitchToFrame, ParaInfo.FindBy, ParaInfo.ObjectId)
        {
        }
    }
    internal class WebCommandSwitchToDefaultContent : WebCommand
    {
        public WebCommandSwitchToDefaultContent()
            : base(IOTypes.Keyboard, EventType.SwitchToDefaultContent)
        {
        }
    }

    internal class WebCommandSetText : WebCommand
    {
        public WebCommandSetText()
            : base(IOTypes.Set, EventType.SetText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.StringValue)
        {
        }
    }
    internal class WebCommandSetDropDownItem : WebCommandSetText
    {
        public WebCommandSetDropDownItem()
            : base()
        {
            Event = EventType.SetDropDownItem;
        }
    }
    internal class WebCommandSetListBoxItems : WebCommandSetText
    {
        public WebCommandSetListBoxItems()
            : base()
        {
            Event = EventType.SetListBoxItems;
        }
    }
    internal class WebCommandSetCheckBoxValue : WebCommand
    {
        public WebCommandSetCheckBoxValue()
            : base(IOTypes.Set, EventType.SetCheckBoxValue, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.IsChecked)
        {
        }
    }

    internal class WebCommandGetText : WebCommand
    {
        public WebCommandGetText()
            : base(IOTypes.Validate, EventType.GetText, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition)
        {
        }
    }
    internal class WebCommandWaitElementExisted : WebCommand
    {
        public WebCommandWaitElementExisted()
            : base(IOTypes.Command, EventType.WaitElementExisted, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.TimeOut)
        {
        }
    }

    internal class WebCommandLaunchApp : WebCommand
    {
        public WebCommandLaunchApp()
            : base(IOTypes.Command, EventType.LaunchApp, ParaInfo.Browser, ParaInfo.Width, ParaInfo.Height, ParaInfo.Url)
        {
        }
    }
    internal class WebCommandNavigate : WebCommand
    {
        public WebCommandNavigate()
            : base(IOTypes.Command, EventType.Navigate, ParaInfo.Url)
        {
        }
    }
    internal class WebCommandConfirm : WebCommand
    {
        public WebCommandConfirm()
            : base(IOTypes.Command, EventType.Confirm, ParaInfo.FindBy, ParaInfo.ObjectId)
        {
        }
    }
    internal class WebCommandCloseWindow : WebCommand
    {
        public WebCommandCloseWindow()
            : base(IOTypes.Command, EventType.CloseWindow)
        {
        }
    }
    internal class WebCommandJavaScript : WebCommand
    {
        public WebCommandJavaScript()
            : base(IOTypes.Command, EventType.JavaScript, ParaInfo.FindBy, ParaInfo.ObjectId, ParaInfo.Condition, ParaInfo.ScriptValue)
        {
        }
    }
}
