﻿using AutoTest.Data;
using mshtml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WebAction
{
    partial class WebActionUI : UserControl
    {
        #region Fields

        private bool _IsBinding;
        private WebAction _WebAction;
        private static Thread _ThreadSpy;
        private ComboBox _CmbFindBy;
        private TextBox _TxtObjectId;

        #endregion

        #region Properties

        private SimpleActionState State
        {
            get { return _WebAction._State; }
        }

        public string Event
        {
            get { return this.State[WebAction.Event]; }
            set { this.State[WebAction.Event] = value; }
        }

        #endregion

        #region Constructors

        public WebActionUI()
        {
            InitializeComponent();

            initEvents();

            cmbAction.SelectionChanged += cmbAction_SelectionChanged;
        }

        #endregion

        #region Events

        void cmbAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SimpleActionState oldState = (SimpleActionState)this.State.Clone();

            if (!_IsBinding)
            {
                this.State.Clear();
                this.Event = oldState[WebAction.Event];
            }

            uiStackParas.Children.Clear();

            WebCommand info = cmbAction.SelectedItem as WebCommand;
            this.Event = info == null ? string.Empty : info.Event.ToString();

            if (info != null && info.Paras != null)
                foreach (var ele in info.Paras)
                {
                    uiStackParas.Children.Add(new Label() { Content = ele.Label, ToolTip = ele.ToolTip, Foreground = Brushes.Gray });
                    string value = oldState[ele.Name];
                    if (ele.Items == null || ele.Items.Count == 0)
                    {
                        TextBox text = new TextBox() { Text = value, Padding = new Thickness(2) };
                        if (ele == ParaInfo.ScriptValue)
                        {
                            text.AcceptsReturn = true;
                            text.AcceptsTab = true;
                            text.Height = 150;
                            text.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                        }
                        text.TextChanged += (s, arg) => { this.State[ele.Name] = ((TextBox)s).Text; };

                        this.State[ele.Name] = value;
                        uiStackParas.Children.Add(text);

                        if (ele.Name == ParaInfo.ObjectId.Name)
                            _TxtObjectId = text;
                    }
                    else
                    {
                        ComboBox cmb = new ComboBox();
                        cmb.Padding = new Thickness(4);
                        cmb.Width = 200;
                        cmb.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        cmb.ItemsSource = ele.Items;
                        cmb.SelectedItem = string.IsNullOrEmpty(value) ? cmb.Items[0] : value;
                        cmb.SelectionChanged += (s, arg) => { this.State[ele.Name] = ((ComboBox)cmb).SelectedItem as string; };

                        this.State[ele.Name] = cmb.SelectedItem as string;

                        if (ele.Name == ParaInfo.FindBy.Name)
                        {
                            Button btn = new Button();
                            btn.Margin = new Thickness(5, 0, 0, 0);
                            btn.Content = "Spy...";
                            btn.Width = 40;
                            btn.Click += spy;

                            StackPanel panel = new StackPanel() { Orientation = System.Windows.Controls.Orientation.Horizontal };
                            panel.Children.Add(cmb);
                            panel.Children.Add(btn);
                            uiStackParas.Children.Add(panel);
                            _CmbFindBy = cmb;
                        }
                        else
                            uiStackParas.Children.Add(cmb);
                    }
                }
        }

        #endregion

        #region Methods

        private void spy(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable(_EnvRequest, EnvironmentVariableTarget.User)))
            //{
            //    MessageBox.Show("Please intall Internet Explorer Spy from the Tools menu.");
            //    return;
            //}

            Clipboard.SetText("AutoTestStart");

            IntPtr handle = FindWindow("IEFrame", null);
            if (handle != IntPtr.Zero)
            {
                if (IsIconic(handle))
                    ShowWindowAsync(handle, 9);
                SetForegroundWindow(handle);
            }
            else
                Process.Start("iexplore");

            if (_ThreadSpy == null)
            {
                _ThreadSpy = new Thread(new ThreadStart(() =>
                    {
                        while (true)
                        {
                            string response = Dispatcher.Invoke<string>(() => { return Clipboard.GetText(); });
                            if (response != null && response.StartsWith("AutoTestStop:"))
                            {
                                response = response.Substring("AutoTestStop:".Length);
                                string[] eles = response.Split((char)1);
                                if (_CmbFindBy != null)
                                    Dispatcher.Invoke(() => { _CmbFindBy.SelectedValue = eles[0]; });
                                if (_TxtObjectId != null)
                                    Dispatcher.Invoke(() => { _TxtObjectId.Text = eles[1]; });
                                break;
                            }
                            Thread.Sleep(500);
                        }
                    }));
                _ThreadSpy.Start();
            }
        }
        public static void StopSpy()
        {
            if (string.Equals(Clipboard.GetText(), "AutoTestStart"))
                Clipboard.SetText("");
            if (_ThreadSpy != null)
            {
                _ThreadSpy.Abort();
                _ThreadSpy = null;
            }
        }

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);
        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);

        private void initEvents()
        {
            ListCollectionView lcv = new ListCollectionView(WebCommand.AllActions);
            lcv.GroupDescriptions.Add(new PropertyGroupDescription("IO"));
            cmbAction.ItemsSource = lcv;
            cmbAction.SelectedIndex = -1;
        }
        public void Bind(WebAction webAction)
        {
            var temp = _WebAction;
            _WebAction = webAction;
            if (object.Equals(temp, webAction) && object.Equals(temp.State, webAction.State))
                return;

            _IsBinding = true;

            uiStackParas.Children.Clear();

            string actionName = this.Event;
            cmbAction.SelectedIndex = -1;
            if (!string.IsNullOrEmpty(actionName))
            {
                bool stop = false;
                for (int i = 0; i < cmbAction.Items.Count && !stop; i++)
                    if (stop = (cmbAction.Items[i] as WebCommand).Event.ToString().Equals(actionName))
                        cmbAction.SelectedIndex = i;
            }

            _IsBinding = false;
        }

        #endregion
    }
}
