﻿using AutoTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WebAction
{
    public class WebActionManager : ActionManager
    {
        public override string Name
        {
            get { return "WebUI Action"; }
        }

        public override ActionBase CreateAction()
        {
            return new WebAction();
        }


        private WebActionUI _ui;
        public override UserControl UI(ActionBase action)
        {
            if (_ui == null)
                _ui = new WebActionUI();
            _ui.Bind(action as WebAction);
            return _ui;
        }
    }
}
