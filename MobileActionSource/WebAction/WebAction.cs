﻿using AutoTest.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using System.Xml.Linq;
using System.Diagnostics;

namespace WebAction
{
    [Serializable]
    public class WebAction : ActionBase
    {
        #region Fields

        internal const string Event = "Event";
        public static TimeSpan TimeOut = new TimeSpan(0, 1, 0);

        #endregion

        #region Properties

        private static IWebDriver _CurrentWebDriver;
        internal static IWebDriver CurrentWebDriver
        {
            get
            {
                if (_CurrentWebDriver == null)
                    throw new Exception(string.Format("Browser has not started"));
                return _CurrentWebDriver;
            }
            set
            {
                _CurrentWebDriver = value;
            }
        }

        internal static DriverService CurrentDriverService { get; set; }

        public override string Name
        {
            get { return "WebUI Action"; }
        }
        
        internal SimpleActionState _State = new SimpleActionState();
        public override IActionState State
        {
            get { return _State; }
        }

        #endregion

        #region Methods

        public override void Execute(IStepInstance step)
        {
            string stEvent = (string)step.StepData[ParaInfo.Event.Name];
            if (string.IsNullOrWhiteSpace(stEvent))
                return;

            step.Log(stEvent != null ? string.Format("Action: {0}", stEvent) : "Action is empty");
            EventType eventType = (EventType)Enum.Parse(typeof(EventType), stEvent);

            step.Log("Executing: ", eventType, "...");

            if (eventType == EventType.LaunchApp)
                GeneralHandler.LauchApp(step, step.StepData);
            else
            {
                float seconds = float.TryParse((step.StepData[ParaInfo.TimeOut.Name] ?? "0").ToString(), out seconds) ? seconds : 0;
                TimeSpan timeOut = seconds > 0 ? TimeSpan.FromMilliseconds(1000 * seconds) : WebAction.TimeOut;
                WebDriverWait wait = new WebDriverWait(WebAction.CurrentWebDriver, timeOut);

                switch (eventType)
                {
                    case EventType.Navigate:
                        step.Result = GeneralHandler.Navigate(wait, step.StepData); break;
                    case EventType.WaitPage:
                        step.Result = GeneralHandler.WaitPage(wait, step.StepData); break;
                    case EventType.CloseWindow:
                        GeneralHandler.CloseWindow(); break;
                    case EventType.SwitchToWindows:
                        KeyboardHandler.SwitchToWindows(wait,step.StepData); break;
                    case EventType.SwitchToDefaultContent:
                        KeyboardHandler.SwitchToDefaultContent(); break;
                    case EventType.TestRail:
                        step.Result = GeneralHandler.TestRail(step.StepData);break;
                    case EventType.API:
                        step.Result = GeneralHandler.API(step.StepData); break;
                    default:
                        executeWebUIAction(wait, step, eventType); break;
                }
            }

            step.Log("Executed");
        }
        private void executeWebUIAction(WebDriverWait wait, IStepInstance step, EventType eventType)
        {
            Value method = step.StepData[ParaInfo.FindBy.Name];
            FindBy findBy = method.IsEmpty() ? FindBy.Id : (FindBy)Enum.Parse(typeof(FindBy), (string)method);
            string valueToFind = (string)step.StepData[ParaInfo.ObjectId.Name];
            By by = findBy == FindBy.Id ? By.Id(valueToFind) :
                findBy == FindBy.Name ? By.Name(valueToFind) :
                findBy == FindBy.TagName ? By.TagName(valueToFind) :
                findBy == FindBy.ClassName ? By.ClassName(valueToFind) :
                findBy == FindBy.CssSelector ? By.CssSelector(valueToFind) :
                findBy == FindBy.XPath ? By.XPath(valueToFind) :
                findBy == FindBy.LinkText ? By.LinkText(valueToFind) : By.PartialLinkText(valueToFind);

            if (eventType == EventType.WaitElementNotExisted)
                step.Result = GeneralHandler.WaitElementNotExisted(wait, step, by);
            else
            {
                int count = 0, total = (int)(wait.Timeout.TotalSeconds/(2*wait.PollingInterval.TotalSeconds)) - 10;
                wait.Until<bool>((driver) =>
                {
                    try
                    {
                        count++;
                        IWebElement element = GeneralHandler.WaitElementExisted(step, by);
                        if (element == null)
                            return false;

                        GeneralHandler.Focus(element);
                        switch (eventType)
                        {
                            case EventType.WaitElementExisted:
                                step.Result = element; break;
                            case EventType.GetText:
                                step.Result = GeneralHandler.GetText(element); break;
                            case EventType.CheckText:
                                step.Result = GeneralHandler.CheckText(element, step.StepData); break;
                            case EventType.WaitText:
                                return (bool)(step.Result = GeneralHandler.CheckText(element, step.StepData));
                            case EventType.SwitchToFrame:
                                KeyboardHandler.SwitchToFrame(element); break;
                            case EventType.Click:
                                MouseHandler.Click(element, step); break;
                            case EventType.DoubleClick:
                                MouseHandler.DoubleClick(element); break;
                            case EventType.Dragdrop:
                                MouseHandler.DragAndDrop(element, step.StepData); break;
                            case EventType.MouseOver:
                                MouseHandler.MouseOver(element); break;
                            case EventType.MouseMove:
                                MouseHandler.MouseMove(element); break;
                            case EventType.TypeText:
                                KeyboardHandler.TypeText(element, step.StepData); break;
                            case EventType.SystemKeyDown:
                                KeyboardHandler.SystemKeyDown(element, step.StepData); break;
                            case EventType.SetText:
                                return GeneralHandler.SetText(element, step.StepData);
                            case EventType.SetDropDownItem:
                                return GeneralHandler.SetDropDownItem(element, step.StepData);
                            case EventType.SetListBoxItems:
                                return GeneralHandler.SetListBoxItems(element, step.StepData);
                            case EventType.SetCheckBoxValue:
                                GeneralHandler.SetCheckBoxValue(element, step.StepData); break;
                            case EventType.Confirm:
                                GeneralHandler.Confirm(element); break;
                            case EventType.JavaScript:
                                step.Result = GeneralHandler.JavaScript(element, step.StepData); break;
                            case EventType.Trigger:
                                GeneralHandler.Trigger(element, step.StepData); break;
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        if (count > total && (eventType == EventType.WaitElementExisted || eventType == EventType.WaitElementNotExisted))
                            return true;
                        return false;
                    }
                });
            }
        }

        public override ActionBase Clone()
        {
            WebAction res = new WebAction();
            res._State = _State.Clone() as SimpleActionState;
            return res;
        }

        public override XElement SaveXml()
        {
            XElement xAction = new XElement("Action", new XAttribute("Type", typeof(WebAction).FullName)
                , new XElement("Name", this.Name));
            XElement xState = new XElement("States");
            xAction.Add(xState);

            foreach (var e in this.State.GetStepParas())
                xState.Add(new XElement("State",
                    new XAttribute("Key", e.Key),
                    new XAttribute("Value", e.Value)));

            return xAction;
        }

        public override void LoadXml(XElement element)
        {
            this.State.LoadXml(element);

            //foreach (var state in element.Element("States").Elements("State"))
            //    this.State.[state.Attribute("Key").Value] = state.Attribute("Value").Value;
        }

        public static void KillWebDriverService()
        {
            if (_CurrentWebDriver != null)
                try { _CurrentWebDriver.Close(); }
                catch { }

            string processName = CurrentDriverService is ChromeDriverService ? "chromedriver" : "IEDriverServer";
            foreach (Process p in Process.GetProcesses().Where(e => e.ProcessName == processName))
                try { p.Kill(); }
                catch { }
            CurrentDriverService = null;
        }

        #endregion
    }
}
